-- MySQL dump 10.14  Distrib 5.5.40-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: mariadb    Database: dominosaz
-- ------------------------------------------------------
-- Server version	5.5.40-MariaDB-1~trusty-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gifts`
--

DROP TABLE IF EXISTS `gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `gamedatetime` datetime NOT NULL,
  `copun` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gifts`
--

LOCK TABLES `gifts` WRITE;
/*!40000 ALTER TABLE `gifts` DISABLE KEYS */;
INSERT INTO `gifts` VALUES (1,6,'2015-09-15 18:10:51','WCB5'),(2,4,'2015-09-15 18:27:58','WCB3'),(3,7,'2015-09-15 18:40:20','WCB1'),(4,7,'2015-09-15 19:44:03','WCB2'),(5,7,'2015-09-16 15:54:57','WCB3'),(6,2,'2015-09-16 16:18:45','WCB1'),(7,7,'2015-09-17 10:53:10','WCB4'),(8,4,'2015-09-17 10:55:23','WCB5'),(9,4,'2015-09-17 10:57:41','WCB5'),(10,1,'2015-09-17 13:01:57','WCB6'),(11,1,'2015-09-17 15:21:57','WCB5'),(12,4,'2015-10-02 11:29:06','WCB4'),(13,14,'2015-10-05 13:48:04','WCB2'),(14,7,'2015-10-06 11:25:22','WCB2'),(15,1,'2015-10-06 11:50:48','WCB5');
/*!40000 ALTER TABLE `gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaders`
--

DROP TABLE IF EXISTS `leaders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaders` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `score` int(40) NOT NULL,
  `picture` varchar(10000) NOT NULL,
  `weekid` int(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaders`
--

LOCK TABLES `leaders` WRITE;
/*!40000 ALTER TABLE `leaders` DISABLE KEYS */;
INSERT INTO `leaders` VALUES (0,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(0,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(0,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(0,4,'Dakshayani Batmanathan',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',NULL),(0,5,'Y\'vonne Ten',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p200x200/11904671_883315618423296_9140911151724611035_n.jpg?oh=97cbda3f3f3007108319845937d3b38c&oe=565D0EEF&__gda__=1453162628_70bef2e18ab2b350ba52b29ddc6129c2',NULL),(0,6,'Tee How',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtl1/v/t1.0-1/p200x200/11951938_10207527778338411_5325055981241606526_n.jpg?oh=55a157bb63789a70966579b0cecb262e&oe=566A016C&__gda__=1449533395_482f87a843c67c47230bba47bbb9496f',NULL),(0,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(0,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(0,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(0,4,'Dakshayani Batmanathan',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',NULL),(0,5,'Y\'vonne Ten',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p200x200/11904671_883315618423296_9140911151724611035_n.jpg?oh=97cbda3f3f3007108319845937d3b38c&oe=565D0EEF&__gda__=1453162628_70bef2e18ab2b350ba52b29ddc6129c2',NULL),(0,6,'Tee How',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtl1/v/t1.0-1/p200x200/11951938_10207527778338411_5325055981241606526_n.jpg?oh=55a157bb63789a70966579b0cecb262e&oe=566A016C&__gda__=1449533395_482f87a843c67c47230bba47bbb9496f',NULL),(0,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(0,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(0,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(0,4,'Dakshayani Batmanathan',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',NULL),(0,5,'Y\'vonne Ten',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p200x200/11904671_883315618423296_9140911151724611035_n.jpg?oh=97cbda3f3f3007108319845937d3b38c&oe=565D0EEF&__gda__=1453162628_70bef2e18ab2b350ba52b29ddc6129c2',NULL),(0,6,'Tee How',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtl1/v/t1.0-1/p200x200/11951938_10207527778338411_5325055981241606526_n.jpg?oh=55a157bb63789a70966579b0cecb262e&oe=566A016C&__gda__=1449533395_482f87a843c67c47230bba47bbb9496f',NULL),(0,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(0,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(0,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(0,4,'Dakshayani Batmanathan',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',NULL),(0,5,'Y\'vonne Ten',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p200x200/11904671_883315618423296_9140911151724611035_n.jpg?oh=97cbda3f3f3007108319845937d3b38c&oe=565D0EEF&__gda__=1453162628_70bef2e18ab2b350ba52b29ddc6129c2',NULL),(0,6,'Tee How',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtl1/v/t1.0-1/p200x200/11951938_10207527778338411_5325055981241606526_n.jpg?oh=55a157bb63789a70966579b0cecb262e&oe=566A016C&__gda__=1449533395_482f87a843c67c47230bba47bbb9496f',NULL),(0,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(0,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(0,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(0,4,'Dakshayani Batmanathan',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',NULL),(0,5,'Y\'vonne Ten',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p200x200/11904671_883315618423296_9140911151724611035_n.jpg?oh=97cbda3f3f3007108319845937d3b38c&oe=565D0EEF&__gda__=1453162628_70bef2e18ab2b350ba52b29ddc6129c2',NULL),(0,6,'Tee How',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtl1/v/t1.0-1/p200x200/11951938_10207527778338411_5325055981241606526_n.jpg?oh=55a157bb63789a70966579b0cecb262e&oe=566A016C&__gda__=1449533395_482f87a843c67c47230bba47bbb9496f',NULL),(0,7,'Daler Aminovich',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11406866_1019720331385147_5726533559944820959_n.jpg?oh=946f4bc0dc19f7644c08efad02a9129b&oe=567007A1&__gda__=1453488963_26b6be792c6b2881c0c1e97c7f2167b1',NULL);
/*!40000 ALTER TABLE `leaders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_results`
--

DROP TABLE IF EXISTS `temp_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_results` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `score` int(40) NOT NULL,
  `picture` varchar(10000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_results`
--

LOCK TABLES `temp_results` WRITE;
/*!40000 ALTER TABLE `temp_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `facebook_id` varchar(200) NOT NULL,
  `email` varchar(500) NOT NULL,
  `dominos_mail` varchar(500) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `picture` varchar(5000) NOT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `registerTimer` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `firstgamedate` datetime DEFAULT NULL,
  `games` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'946076232118191','mokhtar_ss@hotmail.com','mokhtar_ss@hotmail.com','Mukhtar Sayed Saleh','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL,'2015-09-17 09:16:05','2015-09-17 10:03:27',17),(2,'10153200396588995','karxin_91@hotmail.com','','Kar Xinaa','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL,'2015-09-17 09:16:05',NULL,0),(3,'10153327333393052','elaine_chooi@hotmail.com','elaine_chooi@hotmail.com','Elaine Chooi','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL,'2015-09-17 09:16:05',NULL,0),(4,'10207154193076126','shayani18@yahoo.com','shayani18@yahoo.com','Dakshayani Batmanathan','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',NULL,'2015-09-17 09:16:05','2015-10-02 03:25:03',2),(5,'892393014182223','tencheekhuen@gmail.com','','Y\'vonne Ten','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p200x200/11904671_883315618423296_9140911151724611035_n.jpg?oh=97cbda3f3f3007108319845937d3b38c&oe=565D0EEF&__gda__=1453162628_70bef2e18ab2b350ba52b29ddc6129c2',NULL,'2015-09-17 09:16:05',NULL,0),(6,'10207678716191763','tihao90@gmail.com','tihao90@gmail.com','Tee How','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtl1/v/t1.0-1/p200x200/11951938_10207527778338411_5325055981241606526_n.jpg?oh=55a157bb63789a70966579b0cecb262e&oe=566A016C&__gda__=1449533395_482f87a843c67c47230bba47bbb9496f',NULL,'2015-09-17 09:16:05',NULL,0),(7,'1077236958966817','dkendzhaev@gmail.com','dkendzhaev@gmail.com','Daler Aminovich','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11406866_1019720331385147_5726533559944820959_n.jpg?oh=946f4bc0dc19f7644c08efad02a9129b&oe=567007A1&__gda__=1453488963_26b6be792c6b2881c0c1e97c7f2167b1',NULL,'2015-09-17 09:16:05','2015-10-02 04:11:39',20),(8,'10153100823027217','anak_bongsu@hotmail.com','mohdhafidzilmi@gmail.com','Hafidz Ilmi','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpa1/v/t1.0-1/p200x200/1455137_10151733440172217_251996706_n.jpg?oh=038676cc6b60e5c49f4dca5f4963ab24&oe=566583D1&__gda__=1453669269_d5799c65c4c2e069ba9945ee5b73901e',NULL,'2015-09-17 09:16:05','2015-10-05 09:51:26',3),(9,'1215518768473787','najuaminah@yahoo.com','','Najwa Aminah Abas','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p200x200/10590578_959089977450002_4084102624041873366_n.jpg?oh=08a1b06efdc8b374989975bc71a0cc15&oe=56A46074&__gda__=1449438751_8024e864c6b2309a43c0a8882079e77d',NULL,'2015-09-17 09:16:05',NULL,0),(10,'10153396217317740','apekgaijin@yahoo.com','tihao90@gmail.com','Farid Aziz','https://m.ak.fbcdn.net/profile.ak/hprofile-ak-xaf1/v/t1.0-1/s200x200/1454669_10151925713022740_782790144_n.jpg?oh=b1bd6e6a71f7da28457ffeb208c5336d&oe=56662767&__gda__=1449770026_c4da6e1aa4b34f4890a274e8dbd1ebe4',NULL,'2015-09-17 09:16:05',NULL,0),(11,'1204611489552825','abd_rmdn@yahoo.com','','Abd Rmdn','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11377178_1132093066804668_1636657113199134131_n.jpg?oh=3644273c62b70e8e830c7077b2293027&oe=56A288A7&__gda__=1449351749_5169e6847db9ce5d77a40b3cfe144e3b',NULL,'2015-09-17 09:16:05',NULL,0),(12,'10152944844856571','a_liyana89@hotmail.com','a_liyana89@hotmail.com','Assila Liyana Ahmad','https://m.ak.fbcdn.net/profile.ak/hprofile-ak-xpf1/v/t1.0-1/p200x200/11855902_10152860825991571_8879591706541864710_n.jpg?oh=e5d208de78047658ef77dd4b89a8fc21&oe=56A572FF&__gda__=1453816199_c854224430e7643447f76473a694f218',NULL,'2015-09-17 10:02:07',NULL,0),(16,'1139299152765341','yang_tercool@yahoo.com','muhdmuadz.j@gmail.com','Muhd Muadz Jamal','https://m.ak.fbcdn.net/profile.ak/hprofile-ak-prn2/v/t1.0-1/p200x200/533961_676992895662638_1469434670_n.jpg?oh=c6da750eb292b73b4f532b08b9e58ce8&oe=56903B19&__gda__=1451571912_c25ad71138ac40287d09f5240dc476ba',NULL,'2015-10-06 03:46:42','2015-10-06 03:47:24',1),(13,'553913414758448','littleradens@gmail.com','','Syafiqah Mazli','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpl1/v/t1.0-1/p200x200/11953286_545420925607697_7328215377932406469_n.jpg?oh=132aea6added5da45fa00a0a60686ae5&oe=568726EB&__gda__=1453232537_c894ccc1bcde4a9b66a68a97177a1a2f',NULL,'2015-10-02 06:37:58',NULL,0),(14,'10154842470226959','tihanayon@yahoo.com','shayani18@yahoo.com','Tihana Yon','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/10389476_10153035979151959_2658806578532142928_n.jpg?oh=33a929095c1885b245587d6e64a41257&oe=569B136A&__gda__=1452995263_7c1328453b8acb5fb3053fa7e9c31028',NULL,'2015-10-05 05:44:02','2015-10-05 05:45:16',3),(15,'10154212237194908','gslchong@gmail.com','','Geraldine Chong','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfa1/v/t1.0-1/p200x200/1925317_10153774616719908_5016541469469876152_n.jpg?oh=a33ac91479c30a8774ec3f3d37db8a93&oe=56A4870E&__gda__=1452984642_d03866485271964204422c1af4ea0ac4',NULL,'2015-10-05 07:34:15',NULL,0),(17,'518445838313137','umyziera@yahoo.com','','Umy Ziera','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11824949_496023337222054_6757242787441097194_n.jpg?oh=c9c81a4f04175ccc08896478f5fc0626&oe=569EFB92&__gda__=1453035751_777e1eed0d6c0c9ae1af19ada1674627',NULL,'2015-10-06 03:49:33',NULL,0),(18,'894521337264113','tunjithomas54@yahoo.com','allythomas82@yahoo.com','Andrew Thomas','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xta1/v/t1.0-1/s200x200/11800297_866394006743513_2685056805072204903_n.jpg?oh=c39bab4759f1292654a4cc68f59784b2&oe=56CE9F47&__gda__=1456503401_71934798cd079c74d495da34442ed79f',NULL,'2015-10-06 03:56:41','2015-10-06 04:05:22',2),(19,'980309048674826','miawafdhal@hotmail.com','','Seo Hyung Suk','https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/p200x200/11215072_975533422485722_4465969950106693121_n.jpg?oh=43e4d6e902639f9253505c0bd4a2bf75&oe=56871EE3&__gda__=1453300872_03d0655da7a10ab667dc449c45a057bf',NULL,'2015-10-06 04:15:45',NULL,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_lifes`
--

DROP TABLE IF EXISTS `user_lifes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_lifes` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `life` int(1) NOT NULL,
  `source` varchar(100) NOT NULL,
  `life_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_lifes`
--

LOCK TABLES `user_lifes` WRITE;
/*!40000 ALTER TABLE `user_lifes` DISABLE KEYS */;
INSERT INTO `user_lifes` VALUES (1,1,3,'register','2015-09-15 17:22:12'),(2,2,3,'register','2015-09-15 17:41:00'),(3,3,3,'register','2015-09-15 17:41:40'),(105,11,3,'register','2015-09-17 12:05:21'),(5,4,3,'register','2015-09-15 17:46:03'),(6,5,3,'register','2015-09-15 17:47:16'),(7,6,3,'register','2015-09-15 17:48:24'),(104,10,3,'register','2015-09-17 11:33:45'),(11,1,3,'new week','2015-09-15 17:58:40'),(12,2,3,'new week','2015-09-15 17:58:40'),(13,3,3,'new week','2015-09-15 17:58:40'),(14,4,3,'new week','2015-09-15 17:58:40'),(15,5,3,'new week','2015-09-15 17:58:40'),(16,6,3,'new week','2015-09-15 17:58:40'),(17,1,3,'new week','2015-09-15 17:59:00'),(18,2,3,'new week','2015-09-15 17:59:00'),(19,3,3,'new week','2015-09-15 17:59:00'),(20,4,3,'new week','2015-09-15 17:59:00'),(21,5,3,'new week','2015-09-15 17:59:00'),(22,6,3,'new week','2015-09-15 17:59:00'),(23,1,3,'new week','2015-09-15 18:01:51'),(24,2,3,'new week','2015-09-15 18:01:51'),(25,3,3,'new week','2015-09-15 18:01:51'),(26,4,3,'new week','2015-09-15 18:01:51'),(27,5,3,'new week','2015-09-15 18:01:51'),(28,6,3,'new week','2015-09-15 18:01:51'),(29,1,3,'new week','2015-09-15 18:02:34'),(30,2,3,'new week','2015-09-15 18:02:34'),(31,3,3,'new week','2015-09-15 18:02:34'),(32,4,3,'new week','2015-09-15 18:02:34'),(33,5,3,'new week','2015-09-15 18:02:34'),(34,6,3,'new week','2015-09-15 18:02:34'),(103,1,1,'Share facebook','2015-09-17 11:06:44'),(102,1,1,'Share twitter','2015-09-17 11:06:30'),(101,1,-1,'game','2015-09-17 11:02:17'),(39,7,3,'register','2015-09-15 18:31:32'),(41,7,1,'Share facebook','2015-09-15 18:42:05'),(42,7,1,'Share twitter','2015-09-15 18:42:20'),(100,2,1,'Share facebook','2015-09-17 10:59:05'),(99,1,-1,'game','2015-09-17 10:59:01'),(46,7,1,'Share twitter','2015-09-15 19:47:42'),(47,1,3,'new week','2015-09-15 19:59:21'),(48,2,3,'new week','2015-09-15 19:59:21'),(49,3,3,'new week','2015-09-15 19:59:22'),(50,4,3,'new week','2015-09-15 19:59:22'),(51,5,3,'new week','2015-09-15 19:59:22'),(52,6,3,'new week','2015-09-15 19:59:22'),(53,7,3,'new week','2015-09-15 19:59:22'),(98,2,1,'Share facebook','2015-09-17 10:58:35'),(97,4,1,'Share facebook','2015-09-17 10:58:11'),(96,4,-1,'game','2015-09-17 10:56:42'),(95,4,1,'Share twitter','2015-09-17 10:56:34'),(60,7,1,'Share facebook','2015-09-16 15:57:00'),(61,7,1,'Share twitter','2015-09-16 15:57:12'),(94,4,-1,'game','2015-09-17 10:54:14'),(93,4,1,'Share facebook','2015-09-17 10:54:07'),(92,2,-1,'game','2015-09-17 10:53:24'),(68,6,1,'Share facebook','2015-09-16 16:39:06'),(69,6,1,'Share facebook','2015-09-16 16:39:25'),(70,6,1,'Share facebook','2015-09-16 16:41:32'),(71,6,1,'Share facebook','2015-09-16 17:17:32'),(72,8,3,'register','2015-09-16 17:56:28'),(88,1,1,'Share twitter','2015-09-17 10:32:26'),(87,1,1,'Share twitter','2015-09-17 10:32:16'),(91,2,1,'Share facebook','2015-09-17 10:53:18'),(90,7,-1,'game','2015-09-17 10:51:27'),(78,4,1,'Share twitter','2015-09-16 19:31:04'),(79,1,1,'Share twitter','2015-09-16 21:01:29'),(81,7,1,'Share facebook','2015-09-17 09:59:09'),(84,1,1,'Share twitter','2015-09-17 10:31:57'),(83,9,3,'register','2015-09-17 10:14:37'),(106,1,1,'Share twitter','2015-09-17 12:30:50'),(107,7,1,'Share twitter','2015-09-17 12:35:59'),(108,7,-1,'game','2015-09-17 12:36:05'),(109,1,1,'Share twitter','2015-09-17 12:55:10'),(110,1,100,'testing','2015-09-17 00:00:00'),(111,1,-1,'game','2015-09-17 12:56:27'),(112,1,-1,'game','2015-09-17 12:56:58'),(113,1,-1,'game','2015-09-17 13:01:21'),(114,1,-1,'game','2015-09-17 13:05:15'),(115,1,-1,'game','2015-09-17 15:19:55'),(116,1,-1,'game','2015-09-17 15:20:37'),(117,1,-1,'game','2015-09-17 15:20:58'),(118,1,-1,'game','2015-09-17 15:24:24'),(119,1,-1,'game','2015-09-17 16:45:06'),(120,1,-1,'game','2015-09-17 16:45:23'),(121,1,-1,'game','2015-09-17 16:47:11'),(122,1,-1,'game','2015-09-17 16:47:38'),(123,1,-1,'game','2015-09-17 17:29:58'),(124,1,-1,'game','2015-09-17 17:30:36'),(125,1,-1,'game','2015-09-17 17:34:28'),(126,1,-1,'game','2015-09-17 17:42:28'),(127,1,-1,'game','2015-09-17 17:42:35'),(128,1,-1,'game','2015-09-17 17:46:46'),(129,1,-1,'game','2015-09-17 17:49:06'),(130,1,-1,'game','2015-09-17 17:52:12'),(131,1,-1,'game','2015-09-17 17:53:58'),(132,1,-1,'game','2015-09-17 17:56:54'),(133,1,-1,'game','2015-09-17 17:58:56'),(134,1,-1,'game','2015-09-17 18:01:51'),(135,12,3,'register','2015-09-17 18:02:07'),(136,12,-1,'game','2015-09-17 18:02:48'),(137,1,-1,'game','2015-09-17 18:03:33'),(138,1,3,'new week','2015-09-17 19:43:05'),(139,2,3,'new week','2015-09-17 19:43:05'),(140,3,3,'new week','2015-09-17 19:43:05'),(141,4,3,'new week','2015-09-17 19:43:06'),(142,5,3,'new week','2015-09-17 19:43:06'),(143,6,3,'new week','2015-09-17 19:43:06'),(144,7,3,'new week','2015-09-17 19:43:06'),(145,8,3,'new week','2015-09-17 19:43:07'),(146,9,3,'new week','2015-09-17 19:43:07'),(147,10,3,'new week','2015-09-17 19:43:07'),(148,11,3,'new week','2015-09-17 19:43:08'),(149,12,3,'new week','2015-09-17 19:43:08'),(150,1,-1,'game','2015-09-18 11:00:50'),(151,1,-1,'game','2015-09-18 11:03:45'),(152,1,-1,'game','2015-09-18 11:13:33'),(153,1,-1,'game','2015-09-18 11:17:23'),(154,1,1,'Share twitter','2015-09-20 01:06:15'),(155,1,3,'new week','2015-09-25 10:21:45'),(156,2,3,'new week','2015-09-25 10:21:46'),(157,3,3,'new week','2015-09-25 10:21:46'),(158,4,3,'new week','2015-09-25 10:21:46'),(159,5,3,'new week','2015-09-25 10:21:47'),(160,6,3,'new week','2015-09-25 10:21:47'),(161,7,3,'new week','2015-09-25 10:21:47'),(162,8,3,'new week','2015-09-25 10:21:47'),(163,9,3,'new week','2015-09-25 10:21:48'),(164,10,3,'new week','2015-09-25 10:21:48'),(165,11,3,'new week','2015-09-25 10:21:48'),(166,12,3,'new week','2015-09-25 10:21:48'),(167,1,3,'new week','2015-10-01 14:22:16'),(168,2,3,'new week','2015-10-01 14:22:17'),(169,3,3,'new week','2015-10-01 14:22:17'),(170,4,3,'new week','2015-10-01 14:22:17'),(171,5,3,'new week','2015-10-01 14:22:18'),(172,6,3,'new week','2015-10-01 14:22:18'),(173,7,3,'new week','2015-10-01 14:22:18'),(174,8,3,'new week','2015-10-01 14:22:18'),(175,9,3,'new week','2015-10-01 14:22:19'),(176,10,3,'new week','2015-10-01 14:22:19'),(177,11,3,'new week','2015-10-01 14:22:19'),(178,12,3,'new week','2015-10-01 14:22:19'),(179,4,-1,'game','2015-10-02 11:25:09'),(180,4,-1,'game','2015-10-02 11:27:12'),(181,4,-1,'game','2015-10-02 11:27:22'),(182,7,-1,'game','2015-10-02 12:11:45'),(183,13,3,'register','2015-10-02 14:37:58'),(184,14,3,'register','2015-10-05 13:44:02'),(185,14,-1,'game','2015-10-05 13:45:22'),(186,14,-1,'game','2015-10-05 13:49:53'),(187,14,-1,'game','2015-10-05 13:49:58'),(188,14,1,'Share facebook','2015-10-05 13:51:09'),(189,14,-1,'game','2015-10-05 13:51:15'),(190,14,-1,'game','2015-10-05 13:54:10'),(191,7,-1,'game','2015-10-05 14:42:28'),(192,15,3,'register','2015-10-05 15:34:15'),(193,8,-1,'game','2015-10-05 17:51:32'),(194,8,-1,'game','2015-10-05 17:51:43'),(195,8,-1,'game','2015-10-05 17:51:56'),(196,8,-1,'game','2015-10-05 17:52:02'),(197,8,-1,'game','2015-10-05 17:52:09'),(198,8,-1,'game','2015-10-05 17:55:10'),(199,1,-1,'game','2015-10-05 18:17:48'),(200,1,-1,'game','2015-10-05 18:17:59'),(201,1,-1,'game','2015-10-05 18:18:12'),(202,1,-1,'game','2015-10-05 18:18:13'),(203,1,-1,'game','2015-10-05 18:18:20'),(204,1,-1,'game','2015-10-05 18:19:14'),(205,7,-1,'game','2015-10-05 18:20:49'),(206,7,-1,'game','2015-10-05 18:48:31'),(207,7,-1,'game','2015-10-05 18:52:42'),(208,7,-1,'game','2015-10-05 18:53:18'),(209,1,3,'new week','2015-10-06 10:20:10'),(210,2,3,'new week','2015-10-06 10:20:11'),(211,3,3,'new week','2015-10-06 10:20:11'),(212,4,3,'new week','2015-10-06 10:20:11'),(213,5,3,'new week','2015-10-06 10:20:12'),(214,6,3,'new week','2015-10-06 10:20:12'),(215,7,3,'new week','2015-10-06 10:20:12'),(216,8,3,'new week','2015-10-06 10:20:12'),(217,9,3,'new week','2015-10-06 10:20:13'),(218,10,3,'new week','2015-10-06 10:20:13'),(219,11,3,'new week','2015-10-06 10:20:13'),(220,12,3,'new week','2015-10-06 10:20:13'),(221,13,3,'new week','2015-10-06 10:20:14'),(222,14,3,'new week','2015-10-06 10:20:14'),(223,15,3,'new week','2015-10-06 10:20:14'),(224,7,-1,'game','2015-10-06 11:22:57'),(225,1,-1,'game','2015-10-06 11:41:42'),(226,1,-1,'game','2015-10-06 11:41:50'),(227,1,-1,'game','2015-10-06 11:41:51'),(228,16,3,'register','2015-10-06 11:46:42'),(229,16,-1,'game','2015-10-06 11:47:30'),(230,17,3,'register','2015-10-06 11:49:33'),(231,1,1,'Share twitter','2015-10-06 11:49:37'),(232,1,-1,'game','2015-10-06 11:49:43'),(233,18,3,'register','2015-10-06 11:56:41'),(234,18,-1,'game','2015-10-06 12:05:28'),(235,18,-1,'game','2015-10-06 12:05:53'),(236,18,-1,'game','2015-10-06 12:06:07'),(237,18,-1,'game','2015-10-06 12:07:06'),(238,19,3,'register','2015-10-06 12:15:45');
/*!40000 ALTER TABLE `user_lifes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_score`
--

DROP TABLE IF EXISTS `user_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_score` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `score_source` varchar(100) NOT NULL,
  `score` varchar(100) NOT NULL,
  `score_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_score`
--

LOCK TABLES `user_score` WRITE;
/*!40000 ALTER TABLE `user_score` DISABLE KEYS */;
INSERT INTO `user_score` VALUES (1,6,'game','260','2015-09-15 18:10:51'),(2,4,'game','132','2015-09-15 18:27:57'),(3,7,'game','18','2015-09-15 18:40:20'),(4,7,'Share facebook','10','2015-09-15 18:42:05'),(5,7,'Share twitter','10','2015-09-15 18:42:20'),(6,7,'game','104','2015-09-15 19:44:03'),(7,7,'Share twitter','10','2015-09-15 19:47:42'),(8,7,'game','170','2015-09-16 15:54:57'),(9,7,'Share facebook','10','2015-09-16 15:57:00'),(10,7,'Share twitter','10','2015-09-16 15:57:12'),(11,2,'game','38','2015-09-16 16:18:45'),(12,6,'Share facebook','10','2015-09-16 16:39:06'),(13,6,'Share facebook','10','2015-09-16 16:39:25'),(14,6,'Share facebook','10','2015-09-16 16:41:32'),(15,6,'Share facebook','10','2015-09-16 17:17:32'),(16,4,'Share twitter','10','2015-09-16 19:31:04'),(17,1,'Share twitter','10','2015-09-16 21:01:29'),(18,7,'Share facebook','10','2015-09-17 09:59:09'),(19,1,'Share twitter','10','2015-09-17 10:31:57'),(20,1,'Share twitter','10','2015-09-17 10:32:16'),(21,1,'Share twitter','10','2015-09-17 10:32:26'),(22,7,'game','184','2015-09-17 10:53:09'),(23,2,'Share facebook','10','2015-09-17 10:53:18'),(24,4,'Share facebook','10','2015-09-17 10:54:07'),(25,4,'game','252','2015-09-17 10:55:23'),(26,4,'Share twitter','10','2015-09-17 10:56:34'),(27,4,'game','272','2015-09-17 10:57:40'),(28,4,'Share facebook','10','2015-09-17 10:58:11'),(29,2,'Share facebook','10','2015-09-17 10:58:35'),(30,2,'Share facebook','10','2015-09-17 10:59:05'),(31,1,'Share twitter','10','2015-09-17 11:06:30'),(32,1,'Share facebook','10','2015-09-17 11:06:44'),(33,1,'Share twitter','10','2015-09-17 12:30:50'),(34,7,'Share twitter','10','2015-09-17 12:35:59'),(35,1,'Share twitter','10','2015-09-17 12:55:10'),(36,1,'game','300','2015-09-17 13:01:56'),(37,1,'game','266','2015-09-17 15:21:55'),(38,1,'Share twitter','10','2015-09-20 01:06:15'),(39,4,'game','180','2015-10-02 11:29:05'),(40,14,'game','114','2015-10-05 13:48:03'),(41,14,'Share facebook','10','2015-10-05 13:51:09'),(42,7,'game','90','2015-10-06 11:25:21'),(43,1,'Share twitter','10','2015-10-06 11:49:37'),(44,1,'game','252','2015-10-06 11:50:47');
/*!40000 ALTER TABLE `user_score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `week`
--

DROP TABLE IF EXISTS `week`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `week` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `weekname` varchar(100) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `week`
--

LOCK TABLES `week` WRITE;
/*!40000 ALTER TABLE `week` DISABLE KEYS */;
INSERT INTO `week` VALUES (1,'Week 1','2015-10-06 00:00:00','2015-10-12 23:59:59'),(2,'Week 2','2015-10-13 00:00:00','2015-10-19 23:59:59'),(3,'Week 3','2015-10-20 00:00:00','2015-10-26 23:59:59'),(4,'Week 4','2015-10-27 00:00:00','2015-11-02 23:59:59'),(0,'Development week','2015-09-01 00:00:00','2015-10-05 23:59:59');
/*!40000 ALTER TABLE `week` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `winners`
--

DROP TABLE IF EXISTS `winners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `winners` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `score` int(40) NOT NULL,
  `picture` varchar(10000) NOT NULL,
  `weekid` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `winners`
--

LOCK TABLES `winners` WRITE;
/*!40000 ALTER TABLE `winners` DISABLE KEYS */;
INSERT INTO `winners` VALUES (1,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(2,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(3,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(4,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(5,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(6,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(7,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(8,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(9,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(10,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(11,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(12,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(13,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',NULL),(14,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',NULL),(15,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',NULL),(16,1,'Mukhtar Sayed Saleh',1320,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',0),(17,4,'Dakshayani Batmanathan',302,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',0),(18,6,'Tee How',270,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtl1/v/t1.0-1/p200x200/11951938_10207527778338411_5325055981241606526_n.jpg?oh=55a157bb63789a70966579b0cecb262e&oe=566A016C&__gda__=1449533395_482f87a843c67c47230bba47bbb9496f',0),(19,1,'Mukhtar Sayed Saleh',330,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',1),(20,4,'Dakshayani Batmanathan',292,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',1),(21,7,'Daler Aminovich',204,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11406866_1019720331385147_5726533559944820959_n.jpg?oh=946f4bc0dc19f7644c08efad02a9129b&oe=567007A1&__gda__=1453488963_26b6be792c6b2881c0c1e97c7f2167b1',1),(22,1,'Mukhtar Sayed Saleh',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',2),(23,2,'Kar Xinaa',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p200x200/11700796_10153043283228995_8156526407647340205_n.jpg?oh=81b5e1444a4c580947069322c61fa2e8&oe=56A77598&__gda__=1449263680_1d4c7572db5f5a9585dcf837591d7c0b',2),(24,3,'Elaine Chooi',0,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/11056073_10153158106213052_119413795886117756_n.jpg?oh=c84ff7275032e5d03c013af44fbf8910&oe=56A7975F&__gda__=1450281888_1e8778c7913eb5c25f95e88863bddf1e',2),(25,1,'Mukhtar Sayed Saleh',1360,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpt1/v/t1.0-1/p200x200/11178370_925656597493488_8686902243940470380_n.jpg?oh=ad3eaa5999455e071b0e7c1b2643a90d&oe=56A05811&__gda__=1453555820_82771b76716c4fcd2a5741b7c83fb544',0),(26,4,'Dakshayani Batmanathan',302,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/p200x200/11796337_10206764712539356_9195270708223716933_n.jpg?oh=781429d26554dec43e433470dd2afdf7&oe=566BFED9&__gda__=1449796943_6bc735405372e7e406ac277a7dafcc43',0),(27,6,'Tee How',270,'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtl1/v/t1.0-1/p200x200/11951938_10207527778338411_5325055981241606526_n.jpg?oh=55a157bb63789a70966579b0cecb262e&oe=566A016C&__gda__=1449533395_482f87a843c67c47230bba47bbb9496f',0);
/*!40000 ALTER TABLE `winners` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-06  4:22:51
