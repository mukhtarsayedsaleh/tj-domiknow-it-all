<!doctype html>

<html lang="en">
   <head>
      
      <link href="http://allfont.net/allfont.css?fonts=arial-narrow" rel="stylesheet" type="text/css" />

      <meta charset="utf-8">
      <title>Domino's A-Z Game</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      


      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.rwdImageMaps.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     


     

   </head>

  <body>
    
    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>

    <div class="desktopContent hidden-xs">
        <div id="step2">
          <div id="game">
            <div class="headBar">
                <div class="left">
                  <div class="navbutton">
                    <a class="navicon-button x">
                      <div class="navicon"></div>
                    </a>
                  </div>
                  <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
                </div>
                <div class="right">
                <img class="playnowsmall hvr-float" src="assets/img/menu/playsmall.png" />
                </div>
                <img class="logo" src="assets/img/game/logo.png" />
            </div>
            <script type="text/javascript">
            $(function(){
              $('.playnowsmall').click(function(){
                window.location="game.php";
              });
            });
            </script>
          </div>
          <img class="logo img-responsive" src="assets/img/game/pages/tc.png" />
          <div class="tcTextDesktop">
              This Domi-Know-it-all [“Contest”] begins ) 07/10/2015 and ends 17/11/2015 [“the Contest Period”].<br/><br/>

              This Contest is organised by Domino’s Pizza Malaysia (Domino’s Pizza) [“the Organiser”], and the Organiser reserves the right to end or extend the Contest Period at any time without prior notice.<br/><br/>

              This Contest is open to all residents of Malaysia, except employees and immediate family members of the Organiser, and their developer, advertising, promotion, event and PR agencies.<br/><br/>

              <h1>How to play:</h1>
              <ol>
              <li>Before playing the game, users are required to Like the Domino’s Pizza Malaysia Facebook page.</li>
              <li>They are then required to submit their full name, mobile phone number and email address registered on http://www.dominos.com.my</li>
              <li>In the event that the user is not registered on the Domino’s Pizza Malaysia website, they will be required to do so before playing the  game to be eligible to win prizes.</li>
              <li>Users have the opportunity to multiply their scores with any online purchase of Cheese Burst pizzas. In the event that the user did not purchase any Cheese Burst pizzas online, they will only be able to receive points based on the time taken to finish the game.</li>
              <li>The email address submitted will be used to verify the Cheese Burst Pizza purchased online in order to be eligible to win the prizes.</li>
              <li>The objective of the game is to answer the questions correctly in the fastest time.</li>
              <li>Users are required to answer 26 questions within 180 seconds.</li>
              <li>The following are the freebies that the user may redeem according to the time frames stated:
                <ul>
                    <li>If the user completes the game within 180-150 seconds, they are entitled to 50% OFF Cheese Burst Pizza deal (Large)</li>
                    <li>If the user completes the game within 149-120 seconds, they are entitled to 40% OFF Cheese Burst Pizza deal (Regular)</li>
                    <li>If the user completes the game within 119-90 seconds, they are entitled to 20% OFF Cheese Burst Pizza deal (Large)</li>
                    <li>If the user completes the game within 89-60 seconds, they are entitled to 20% OFF Cheese Burst Pizza deal (Regular)</li>
                    <li>If the user completes the game within 59-30 seconds, they are entitled to 10% OFF Cheese Burst Pizza deal (Large)</li>
                    <li>If the user completes the game within 29-0 seconds, they are entitled to 10% OFF Cheese Burst Pizza deal (Regular)</li>
                    <li>Free product is not exchangeable for cash.</li>

                </ul>
              </li>
              <li>An email will be sent to the user, and the freebies will be credited to the user profile on the Domino’s Pizza Malaysia website  as ecoupons.</li>
              <li>The user score in the running for the contest will be based on their highest score submitted.</li>
              <li>Participants do not need to make a purchase of the Cheese Burst pizza in order to be eligible to win the prizes.</li>
              <li>Participants are able to multiply their score with the purchase of Cheese Burst Crust Pizzas. <br/>
                    1 REGULAR Cheese Burst Crust Pizza = X2 Multiplier<br/>
                    1 LARGE Cheese Burst Crust Pizza = X3 Multiplier<br/>
              </li>
              <li>Weekly winners will be based on the highest scores, which are calculated by multiplying the highest game score of the week obtained in the contest by the multiplier (if any).<br/>
                      Formula: [Highest game score for the week (including share points)] x [Multipliers collected (if any)] = Final Score<br/><br/>
                      E.G: 1,000 points [Highest game score for the week (including share points)] x 2 (if 1 REGULAR Cheese Burst Crust is purchased) = 2,000 points<br/><br/>
                      In the event of a tie, priority will be given to the contestant with the highest number of Cheese Burst Crust pizzas purchased. (Highest number of multipliers)
                    </li>
              <li>Game scores are calculated by the time taken to complete the game according to the countdown timer, with 1 second remaining translating to 2 points. E.g. If a player completes the game within 30 seconds, the countdown timer that starts with 45 seconds will then display 15 seconds at the end of the game. The number shown on the countdown timer translates to the number of points earned, multiplied by 2.</li>
              <li>In order to earn an extra 10 points, users are allowed to share the game on their Facebook and Twitter</li>
              <li>Winners will be determined weekly and game scores for all participants will be refreshed to 0 weekly.</li>
              </ol>
              
              <h1>Prizes:</h1>
              <table class="prizestable">
                <tr>
                    <th>Prizes</th>
                    <th>Week 1</th>
                    <th>Week 2</th>
                    <th>Week 3</th>
                    <th>Week 4</th>
                    <th>Week 5</th>
                    <th>Week 6</th>
                </tr>
                <tr>
                    <td>1st Place</td>
                    <td>1 year supply of Xtra Large Pizza + 2 invites to Top Secret Party</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                </tr>
                <tr>
                    <td>2nd Place</td>
                    <td>1 year supply of Large Pizza + 2 invites to Top Secret Party</td>
                    <td>1 year supply of Large Pizza</td>
                    <td>1 year supply of Large Pizza</td>
                    <td>1 year supply of Large Pizza</td>
                    <td>1 year supply of Large Pizza</td>
                    <td>1 year supply of Large Pizza</td>
                </tr>
                <tr>
                    <td>3rd Place</td>
                    <td>1 year supply of Regular Pizza + 2 invites to Top Secret Party</td>
                    <td>1 year supply of Regular Pizza</td>
                    <td>1 year supply of Regular Pizza</td>
                    <td>1 year supply of Regular Pizza</td>
                    <td>1 year supply of Regular Pizza</td>
                    <td>1 year supply of Regular Pizza</td>
                </tr>
              </table>

              <br/>
              All prizes will be credited to your Domino’s Online Account and is only valid for Take Away.<br/>
              * Free supply of pizza is limited to only 1 Pizza of your choice per month. Example: 1 year free supply of pizza = 12 pizzas (1 per month)<br/>
              * All prizes will be credited to the Profile and only valid for online order and take away.<br/>
              The Organiser reserves the right to substitute any of the prizes with that of similar value at any time without prior notice and all prizes are non-transferable and not exchangeable for cash.<br/>
              Acceptance of prizes constitutes permission to the Organiser and its agencies to use winners’ names and/or photographs for purposes of publicity, advertising and/or trade without further notice or compensation.<br/>
              From the 7th of October until the 13th of October, 5 contestants will have the opportunity to win special invites to the Top Secret Party.<br/>
              Each winner will get 2 invites.<br/>
              Acceptance of the invites to the Top Secret Party constitutes permission to the Organiser and its agencies to use winners’ names and/or photographs for purposes of publicity, advertising and/or trade without further notice or compensation.<br/>
              Judges’ decisions are final and no correspondence thereon will be entertained.<br/>
              By participating in this Contest, participants agree to be bound by this Contest Rules &amp; Regulations and the decisions of the Organiser.<br/>
              <h1>Freebies:</h1>
              <ol>
                <li>Freebies will be credited as eCoupon into the user’s Domino’s account.</li>
                <li>Valid for 14 days upon being credited into user’s Domino’s account.</li>
                <li>Valid for online purchases only.</li>
                <li>Only one (1) coupon may be used per order.</li>
                <li>Surcharge applies for Cheese Burst Pizzas</li>
              </ol>

              <h1>Others:</h1>
              <ol>
                <li>This Contest is in no way sponsored, endorsed or administered by, or associated with Facebook.</li>
                <li>This Contest adheres to Facebook rules and regulations when it goes live on Domino’s Pizza Malaysia Facebook page but if at any time after the Contest goes live, Facebook changes its rules and regulations, Domino’s Pizza Malaysia will amend the Contest structure to suit the changes implemented.</li>
                <li>Only Contestants who fulfil the Terms and Conditions shall be eligible to win a Prize. The Organiser reserves the right to disqualify Contestants who have failed to fulfil the Terms and Conditions and/or Contestants who have submitted incomplete or inaccurate entries from the Contest, without prior notice.</li>
                <li>The Organiser (including its respective employees, staff and agents) shall not be liable for any loss (including loss of opportunity and consequential loss arising therewith) and/or any damage suffered by any Contestant or Winner in connection with the Contest.</li>
                <li>The Winners are bound by the Terms and Conditions that come with the Prize. The acceptance of Prize indicates the acknowledgment and agreement of such Terms and Conditions.</li>
                <li>No responsibility or liability is assumed by Domino’s Pizza Malaysia and its Administrators for unauthorized human or non-human intervention in the operation of the contest, including without limitation, unauthorized tampering, hacking, theft, virus, bugs, worms or destruction of any aspect of the contest, or loss, miscount, misdirection, inaccessibility or unavailability of a participant’s entries in the contest.</li>
                <li>We reserve the right to disqualify and make invalid any player whom we deem is manipulating and/or gaming the system.</li>
                <li>The Organiser is absolved from all liability resulting from the Contestants’ infringement of third party copyrights from any form of submissions.</li>
                <li>The Prize is not exchangeable and/or transferable. Failure to accept Prize by the Winners when required to do so shall constitute a rejection by such Winner(s) and the Organiser reserves it rights to award the Prize to another Winner(s).</li>
                <li>The Organiser reserves its sole right and discretion to delete, remove, not consider or reject content that is deemed by it to be improper or offensive in whatever nature.</li>
                <li>The Organiser reserves its rights to publish or display materials or information, including but not limited to the names of all Contestants for marketing, advertising and publicity purposes in any manner it deems appropriate. The rights of the entries are automatically assigned to the Organiser and that it reserves right to utilise the entries via any means without compensation</li>
                <li>The Organiser further reserves its right to use any personal data of Contestants in any manner and/or for any purpose it deems fit and the Contestants are deemed to consent to such use with no monetary payment.</li>
                <li>The Organiser reserves its right to cancel, terminate or suspend the Contest with or without any prior notice and reason. For the avoidance of doubt, cancellation, termination or suspension by the Organiser shall not entitle the Contestants to any claim or compensation against the Organiser for any and all losses or damages suffered or incurred as a direct or indirect result of the act of cancellation, termination or suspension.</li>
                <li>The Terms and Conditions herein shall prevail over any inconsistent terms, conditions, provisions or representations contained in any other promotional or advertising materials for the Contest.</li>
                <li>The Organiser reserves its right to vary, delete or add to any of these Terms and Conditions and/or substitute or replace the Prizes from time to time without any prior notice.</li>
                <li>By participating in the Contest, it is deemed that the Contestants agree to be bound by the terms and subject to the conditions herein set out upon submission of entry.</li>
                <li>The decisions of the Organiser in relation to every aspect of the Contest including but not limited to the type of Prize and Winner(s) shall be deemed final and conclusive under any circumstance and no complaint from any Contestant will be entertained. The decision of judges appointed by the Organiser are final, conclusive and binding and no further appeal, enquiry and/or correspondence will be entertained.</li>
              </ol>

          </div>
        </div>
    </div>
































    <div id="mobileGame" class="mobileContent visible-xs">
        <img id="mapfixer" class="img-responsive" src="assets/img/game/mobile/head0.png" usemap="#mobilemap" />
        <map name="mobilemap">
          <area shape="rect" coords="0,21,33,46" class="order" href="menu.php">
        </map>
        <img class="playerImage" />
        <p class="playerName2"></p>
        
        <img class="logo img-responsive" style="margin-top:30px;" src="assets/img/game/pages/tcmobile.png" />
        <div class="tcmobile">
             This Domi-Know-it-all [“Contest”] begins XX/XX/2015 and ends XX/XX/2015 [“the Contest Period”].<br/><br/>

              This Contest is organised by Domino’s Pizza Malaysia Pte Ltd (Domino’s Pizza) [“the Organiser”], and the Organiser reserves the right to end or extend the Contest Period at any time without prior notice.<br/><br/>

              This Contest is open to all residents of Malaysia, except employees and immediate family members of the Organiser, and their developer, advertising, promotion, event and PR agencies.<br/><br/>

              <h1>How to play:</h1>
              <ol>
              <li>Before playing the game, users are required to Like the Domino’s Pizza Malaysia Facebook page.</li>
              <li>They are then required to submit their full name, mobile phone number and email address registered on http://www.dominos.com.my</li>
              <li>In the event that the user is not registered on the Domino’s Pizza Malaysia website, they will be required to do so before playing the  game to be eligible to win prizes.</li>
              <li>Users have the opportunity to multiply their scores with any online purchase of Cheese Burst pizzas. In the event that the user did not purchase any Cheese Burst pizzas online, they will only be able to receive points based on the time taken to finish the game.</li>
              <li>The email address submitted will be used to verify the Cheese Burst Pizza purchased online in order to be eligible to win the prizes.</li>
              <li>The objective of the game is to answer the questions correctly in the fastest time.</li>
              <li>Users are required to 26 questions within 180 seconds.</li>
              <li>The following are the freebies that the user may redeem according to the time frames stated:
                <ul>
                    <li>If the user completes the game within XX-XX seconds, they are entitled to 50% OFF Cheese Burst Pizza deal (Large)</li>
                    <li>If the user completes the game within XX-XX seconds, they are entitled to 40% OFF Cheese Burst Pizza deal (Regular)</li>
                    <li>If the user completes the game within XX-XX seconds, they are entitled to 20% OFF Cheese Burst Pizza deal (Large)</li>
                    <li>If the user completes the game within XX-XX seconds, they are entitled to 20% OFF Cheese Burst Pizza deal (Regular)</li>
                    <li>If the user completes the game within XX-XX seconds, they are entitled to 10% OFF Cheese Burst Pizza deal (Large)</li>
                    <li>If the user completes the game within XX-XX seconds, they are entitled to 20% OFF Cheese Burst Pizza deal (Regular)</li>
                    <li>Free product is not exchangeable for cash.</li>
                </ul>
              </li>
              <li>An email will be sent to the user, and the freebies will be credited to the user profile on the Domino’s Pizza Malaysia website  as ecoupons.</li>
              <li>The user score in the running for the contest will be based on their highest score submitted.</li>
              <li>Participants do not need to make a purchase of the Cheese Burst pizza in order to be eligible to win the prizes.</li>
              <li>Weekly winners will be based on the highest scores, which are calculated by multiplying the highest game score of the week obtained  in the contest by the number of Cheese Burst Pizzas purchased.</li>
              <li>Game scores are calculated by the time taken to complete the game according to the countdown timer, with 1 second remaining translating to 2 points. E.g. If a player completes the game within 30 seconds, the countdown timer that starts with 45 seconds will then display 15 seconds at the end of the game. The number shown on the countdown timer translates to the number of points earned, multiplied by 2.</li>
              <li>In order to earn an extra 10 points, users are allowed to share the game on their Facebook and Twitter</li>
              </ol>
              
              <h1>Prizes:</h1>
              <table class="prizestable">
                <tr>
                    <th>Prizes</th>
                    <th>Week 1</th>
                    <th>Week 2</th>
                    <th>Week 3</th>
                    <th>Week 4</th>
                </tr>
                <tr>
                    <td>1st Place</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                    <td>1 year supply of Xtra Large Pizza</td>
                </tr>
                <tr>
                    <td>2nd Place</td>
                    <td>1 year supply of Large Pizza</td>
                    <td>1 year supply of Large Pizza</td>
                    <td>1 year supply of Large Pizza</td>
                    <td>1 year supply of Large Pizza</td>
                </tr>
                <tr>
                    <td>3rd Place</td>
                    <td>1 year supply of Regular Pizza</td>
                    <td>1 year supply of Regular Pizza</td>
                    <td>1 year supply of Regular Pizza</td>
                    <td>1 year supply of Regular Pizza</td>
                </tr>
              </table>

              <br/>
              * Free supply of pizza is limited to only 1 Pizza of your choice per month. Example: 1 year free supply of pizza = 12 pizzas (1 per month)<br/>
              * All prizes will be credited to the Profile and only valid for online order and take away.<br/>
              The Organiser reserves the right to substitute any of the prizes with that of similar value at any time without prior notice and all prizes are non-transferable and not exchangeable for cash.<br/>
              Acceptance of prizes constitutes permission to the Organiser and its agencies to use winners’ names and/or photographs for purposes of publicity, advertising and/or trade without further notice or compensation.<br/>
              Judges’ decisions are final and no correspondence thereon will be entertained.<br/>
              By participating in this Contest, participants agree to be bound by this Contest Rules &amp; Regulations and the decisions of the Organiser.<br/>
              <h1>Freebies:</h1>
              <ol>
                <li>Freebies will be credited as eCoupon into the user’s Domino’s account.</li>
                <li>Valid for 14 days upon being credited into user’s Domino’s account.</li>
                <li>Valid for online purchases only.</li>
                <li>Only one (1) coupon may be used per order.</li>
                <li>Surcharge applies for Cheese Burst Pizzas</li>
              </ol>

              <h1>Others:</h1>
              <ol>
                <li>This Contest is in no way sponsored, endorsed or administered by, or associated with Facebook.</li>
                <li>This Contest adheres to Facebook rules and regulations when it goes live on Domino’s Pizza Malaysia Facebook page but if at any time after the Contest goes live, Facebook changes its rules and regulations, Domino’s Pizza Malaysia will amend the Contest structure to suit the changes implemented.</li>
                <li>Only Contestants who fulfil the Terms and Conditions shall be eligible to win a Prize. The Organiser reserves the right to disqualify Contestants who have failed to fulfil the Terms and Conditions and/or Contestants who have submitted incomplete or inaccurate entries from the Contest, without prior notice.</li>
                <li>The Organiser (including its respective employees, staff and agents) shall not be liable for any loss (including loss of opportunity and consequential loss arising therewith) and/or any damage suffered by any Contestant or Winner in connection with the Contest.</li>
                <li>The Winners are bound by the Terms and Conditions that come with the Prize. The acceptance of Prize indicates the acknowledgment and agreement of such Terms and Conditions.</li>
                <li>No responsibility or liability is assumed by Domino’s Pizza Malaysia and its Administrators for unauthorized human or non-human intervention in the operation of the contest, including without limitation, unauthorized tampering, hacking, theft, virus, bugs, worms or destruction of any aspect of the contest, or loss, miscount, misdirection, inaccessibility or unavailability of a participant’s entries in the contest.</li>
                <li>We reserve the right to disqualify and make invalid any player whom we deem is manipulating and/or gaming the system.</li>
                <li>The Organiser is absolved from all liability resulting from the Contestants’ infringement of third party copyrights from submission of photos and captions.</li>
                <li>The Prize is not exchangeable and/or transferable. Failure to accept Prize by the Winners when required to do so shall constitute a rejection by such Winner(s) and the Organiser reserves it rights to award the Prize to another Winner(s).</li>
                <li>The Organiser reserves its sole right and discretion to delete, remove, not consider or reject content that is deemed by it to be improper or offensive in whatever nature.</li>
                <li>The Organiser reserves its rights to publish or display materials or information, including but not limited to the names of all Contestants for marketing, advertising and publicity purposes in any manner it deems appropriate. The rights of the entries are automatically assigned to the Organiser and that it reserves right to exploit the entries via any means without compensation</li>
                <li>The Organiser further reserves its right to use any personal data of Contestants in any manner and/or for any purpose it deems fit and the Contestants are deemed to consent to such use with no monetary payment.</li>
                <li>The Organiser reserves its right to cancel, terminate or suspend the Contest with or without any prior notice and reason. For the avoidance of doubt, cancellation, termination or suspension by the Organiser shall not entitle the Contestants to any claim or compensation against the Organiser for any and all losses or damages suffered or incurred as a direct or indirect result of the act of cancellation, termination or suspension.</li>
                <li>The Terms and Conditions herein shall prevail over any inconsistent terms, conditions, provisions or representations contained in any other promotional or advertising materials for the Contest.</li>
                <li>The Organiser reserves its right to vary, delete or add to any of these Terms and Conditions and/or substitute or replace the Prizes from time to time without any prior notice.</li>
                <li>By participating in the Contest, it is deemed that the Contestants agree to be bound by the terms and subject to the conditions herein set out upon submission of entry.</li>
                <li>The decisions of the Organiser in relation to every aspect of the Contest including but not limited to the type of Prize and Winner(s) shall be deemed final and conclusive under any circumstance and no complaint from any Contestant will be entertained. The decision of judges appointed by the Organiser are final, conclusive and binding and no further appeal, enquiry and/or correspondence will be entertained.</li>
                <li>During the course of the campaign period, each person can only win one (1) time. There will not be a repetition of a winner. The organizer reserves final rights to determine each weekly winner without compensation and no plea will be entertained.</li>
              </ol>

        </div>
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 

    </div>

    <!-- <img class="circlelogoutbtn btn-menu-mobile visible-xs" src="assets/img/logoutcircle.png" /> -->

      
    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});
    </script>

    <script type="text/javascript">

      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };

      // global variables
      var user=sessionStorage.getObj('tjuser');

      // //prevent cheating
      // if(user == null)
      // {
      //   logoutFromTheGame();
      // }
     
      // load user info
      if(user != null)
      {
        $('.playerImage').attr('src',user.picture);
        $('.playerName').html(user.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
        $('.playerName2').html(user.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');
      }

      function logoutFromTheGame()
      {
        user = '-1';
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#step2').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#step2mobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }

      
      
      $(function(){
          $('#mapfixer').rwdImageMaps();

          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });

          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          $('.facebookconnect').click(function(e){
            e.preventDefault();
            $('body').addClass("loading");
            window.location='game.php';
          });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });
      });
    </script>

   

  </body>
</html>
    
