<!doctype html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Domino's A-Z Game</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">
      <link rel="stylesheet" href="assets/js/fancybox/source/jquery.fancybox.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      

      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.rwdImageMaps.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>
      <script src="assets/js/fancybox/source/jquery.fancybox.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     


     

   </head>

  <body>

    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>

    <div class="desktopContent hidden-xs">
        <div id="game">
          <div class="headBar">
              <div class="left">
                <div class="navbutton">
                  <a class="navicon-button x">
                    <div class="navicon"></div>
                  </a>
                </div>
                <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
              </div>
              <div class="right">
                <div class="level" data-level="0">
                  <img class="playerImage" src="" />
                  <p class="playerName"></p>
                </div>
              </div>
              <img class="logo" src="assets/img/game/logo.png" />
          </div>
          <div class="headBar headBar2">
              <div class="left">
              </div>
              <div class="right">
                <div class="lifes" data-lifes="3">
                  <h1>5</h1>
                </div>
              </div>
              <div class="timeCounter">
                <h1 class="gameTime">0</h1>
                <h2>Seconds</h2>
              </div>
          </div>

          

          <img class="timeoutimg" src="assets/img/game/timeout.png" />

          <img class="hvr-float playagaingame" style="margin-top:-70px !important;" src="assets/img/game/popups/playagain.png" />
         
        </div>
    </div>
































    <div id="mobileGame" class="mobileContent visible-xs">
        <img id="mapfixer" class="img-responsive" src="assets/img/game/mobile/head0.png" usemap="#mobilemap" />
        <map name="mobilemap">
          <area shape="rect" coords="0,21,33,46" class="order" href="menu.php">
        </map>
        <img class="playerImage" />
        <p class="playerName2"></p>
          
       <div class="gameBody">
          
          <div style="width:100%;" class="left">
              <div class="lifes" data-lifes="3">
                <h1>5</h1>
              </div>
              <div class="timeCounter">
                <h1 class="gameTime">0</h1>
                <h2>Seconds</h2>
              </div>
              <br/><br/>
              <br/><br/>
               <img class="timeoutimg img-responsive" src="assets/img/game/timeout_mobile.png" />

              <img class="hvr-float playagaingame" style="margin-top:-70px !important;" src="assets/img/game/popups/playagain.png" />
         
          </div>

       </div>
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 

    </div>

    <!-- <img class="circlelogoutbtn2 btn-menu-mobile visible-xs" src="assets/img/logoutcircle.png" /> -->

      











































































      <!-- popups -->
      <div style="display:none;" id="sharedalreadypopup">
        <img class="img-responsive hidden-xs" src="assets/img/game/popups/sharedalready.png" />
        <img class="img-responsive visible-xs" src="assets/img/game/popups/sharedalready_mobile.png" />
      </div>


      <div style="display:none;" id="sharepopup">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/share.png" />
          <img id="shareface" class="hvr-float shareface" src="assets/img/game/popups/shareface.png" />
          <img id="sharetwitter" class="hvr-float sharetwitter" src="assets/img/game/popups/sharetwitter.png" />
        </div>
        <div class="visible-xs">
          <img class="img-responsive" src="assets/img/game/popups/share_mobile.png" />
          <img class="shareface centerShare" src="assets/img/game/popups/shareface.png" />
          <img class="sharetwitter centerShare" src="assets/img/game/popups/sharetwitter.png" />
        </div>
      </div>



      <div style="display:none;" id="gameover">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/gameover.png" />
          <img class="hvr-float playagaingame" src="assets/img/game/popups/playagain.png" />
        </div>
        <div class="visible-xs">
           <img class="img-responsive" src="assets/img/game/popups/gameover_mobile.png" />
          <img class="hvr-float playagaingame" src="assets/img/game/popups/playagain.png" />
        </div>
      </div>




      <div style="display:none;" id="nomrelifes">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/nomorepizza.png" />
          <img id="shareface" class="hvr-float shareface" src="assets/img/game/popups/shareface.png" />
          <img id="sharetwitter" class="hvr-float sharetwitter" src="assets/img/game/popups/sharetwitter.png" />
        </div>
        <div class="visible-xs">
          <img class="img-responsive" src="assets/img/game/popups/nomorepizza_mobile.png" />
          <img class="shareface centerShare" src="assets/img/game/popups/shareface.png" />
          <img class="sharetwitter centerShare" src="assets/img/game/popups/sharetwitter.png" />
        </div>
      </div>


      <!-- popups end -->












































































    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});

      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };

      // global variables
      var user=sessionStorage.getObj('tjuser');
      var gameTime = 0;
      var tickInterval;
      //prevent cheating
      if(user == null)
      {
        logoutFromTheGame();
      }
     
      // load user info
      $('.playerImage').attr('src',user.picture);
      $('.playerName').html(user.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
      $('.playerName2').html(user.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');

      function logoutFromTheGame()
      {
        user = '-1';
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#game').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#gamemobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }


    
      
      
      $(function(){

          //initializeing the game
          $.post('engine/init.php',{'u':user.email},function(data){
            $('body').removeClass('loading');
            userlifes = data.lifes;
            $('.lifes h1').html(userlifes);
            if(userlifes <= 0)
            {
              $('.lifes h1').html('0');
              $('.lifes').attr('data-lifes',0);
            }
          },"json");

         
          $('#mapfixer').rwdImageMaps();

          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });
          


          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          // $('.shareface').click(function(e){
          //   e.preventDefault();
          //   alert('share facebook');
          // });

          // $('.sharetwitter').click(function(e){
          //   e.preventDefault();
          //   alert('share twitter');
          // });

          $('.playagaingame').click(function(e){
            e.preventDefault();
            window.location='game.php';
          });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });

          // $.fancybox.open($("#nomrelifes"),
          //     {
          //         topRatio:0.5,
          //         maxWidth: 900,
          //         width: 900,
          //         'afterClose' : function () { 
                   
          //         },
          //         helpers : {
          //           overlay : {
          //             css : {
          //               'background' : 'rgba(0, 0, 0, 0.3)'
          //             }
          //           }
          //         }  
          // });
          
      });
    </script>




  </body>
</html>
    
