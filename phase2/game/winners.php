<?php 
  include('engine/config.php');
  //getting all campaign weeks
      // defining
      //     $_weeks=array();

      // get current week id for checking purposes
          $week = ORM::for_table(null)->raw_query('SELECT `id`, `weekname` , date(start) as `start`, date(end) as `end` FROM week where now() >= start and now() <= end')->find_one();
          $thisweekid = $week['id'];
          if($thisweekid == '')
            $thisweekid =5;
      
?>
<!doctype html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Domino's A-Z Game</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      


      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.rwdImageMaps.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     


     

   </head>

  <body>
    
    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>

    <div class="desktopContent hidden-xs">
        <div id="step2">
          <div id="game">
            <div class="headBar">
                <div class="left">
                  <div class="navbutton">
                    <a class="navicon-button x">
                      <div class="navicon"></div>
                    </a>
                  </div>
                  <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
                </div>
                <div class="right">
                <!-- <img class="playnowsmall hvr-float" src="assets/img/menu/playsmall.png" /> -->
                </div>
                <img class="logo" src="assets/img/game/logo.png" />
            </div>
            <script type="text/javascript">
            $(function(){
              $('.playnowsmall').click(function(){
                window.location="game.php";
              });
            });
            </script>
          </div>
          
          <img class="logo img-responsive" src="assets/img/game/pages/winner.png" />
          
        </div>

        <div class="winners">
          <div class="leaderWeeks">
            <div data-weekid="1" class="singleWeek">Week 1</div>
            <div data-weekid="2" class="singleWeek">Week 2</div>
            <div data-weekid="3" class="singleWeek">Week 3</div>
            <div data-weekid="4" class="singleWeek">Week 4</div>
            <div data-weekid="5" class="singleWeek active">Week 5</div>
            <div data-weekid="6" class="singleWeek">Week 6</div>
          </div>
          <div class="leaderContent">
              

              <img class="mainimage logo img-responsive" src="assets/img/game/pages/winners_week2.png" />

              <!-- <img class="circlesImage" src="assets/img/game/pages/winners1desktop.png" />
              
              <img class="winner2desktop" src="assets/img/game/unknown.png" />
              <img class="winner1desktop" src="assets/img/game/unknown.png" />
              <img class="winner3desktop" src="assets/img/game/unknown.png" />
              <h1 class="name1desktop">?</h1>
              <h1 class="name2desktop">?</h1>
              <h1 class="name3desktop">?</h1 -->
          </div>
        </div>

        <img class="winnersdown" src="assets/img/game/pages/winnersdown.png" />
        <img class="playnowsmall playnowsmallfixer hvr-float" src="assets/img/menu/playsmall.png" />

    </div>
































    <div id="mobileGame" class="mobileContent visible-xs">
        <img id="mapfixer" class="img-responsive" src="assets/img/game/mobile/head0.png" usemap="#mobilemap" />
        <map name="mobilemap">
          <area shape="rect" coords="0,21,33,46" class="order" href="menu.php">
        </map>
        <img class="playerImage" />
        <p class="playerName2"></p>
        
        <img class="logo img-responsive" src="assets/img/game/pages/winnermobile.png" />
        
        <div class="winnersMobile">
        
            <div class="leaderWeeks">
              <div data-weekid="1" class="singleWeek">Week 1</div>
              <div data-weekid="2" class="singleWeek">Week 2</div>
              <div data-weekid="3" class="singleWeek">Week 3</div>
              <div data-weekid="4" class="singleWeek">Week 4</div>
              <div data-weekid="5" class="singleWeek active">Week 5</div>
              <div data-weekid="6" class="singleWeek">Week 6</div>
            </div>


            <div class="leaderContent">
                
              <img class="mainimage2 logo img-responsive" src="assets/img/game/pages/winners_week2_m.png" />
                <!-- <img class="circlesImage2" src="assets/img/game/pages/winners1mobile.png" /> -->
              <!-- 
                <img class="winner1mobile" src="assets/img/game/unknown.png" />
                <img class="winner2mobile" src="assets/img/game/unknown.png" />
                <img class="winner3mobile" src="assets/img/game/unknown.png" />
                <h1 class="name1mobile">?</h1>
                <h1 class="name2mobile">?</h1>
                <h1 class="name3mobile">?</h1> -->

            </div>
        </div>
        <br/>
        <img class="logo img-responsive" src="assets/img/game/pages/winnermobile2.png" />

      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 

    </div>

    <!-- <img class="circlelogoutbtn btn-menu-mobile visible-xs" src="assets/img/logoutcircle.png" /> -->

      
    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});

      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };

      // global variables
      var user=sessionStorage.getObj('tjuser');

      // //prevent cheating
      // if(user == null)
      // {
      //   logoutFromTheGame();
      // }
     
      // load user info
      if(user != null)
      {
        $('.playerImage').attr('src',user.picture);
        $('.playerName').html(user.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
        $('.playerName2').html(user.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');
      }

      function logoutFromTheGame()
      {
        user = '-1';
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#step2').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#step2mobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }

      
      
      $(function(){


          var weeks = <?php echo json_encode($_weeks); ?>;
          var thisweekid = <?php echo $thisweekid; ?>;
          console.log(weeks);
          $('.singleWeek').click(function(e){
            var id = $(this).data('weekid');
            if(id > 5)
                { 
                  //no winners for this week yet;
                  // console.log('No winners for this week yet');
                  // $('.desktopContent .winners .leaderContent').html('<h1 class="errmessageforwinners">No winners yet</h1>');
                  // $('#mobileGame .winnersMobile .leaderContent').html('<h1 class="errmessageforwinners">No winners yet</h1>');
                  return;
                }
                $('.singleWeek').removeClass('active');
                $(this).addClass('active');

               

                //alert('assets/img/game/pages/winners_week'+id+'.png');
                $('.mainimage').attr('src','assets/img/game/pages/winners_week'+id+'.png');
                $('.mainimage2').attr('src','assets/img/game/pages/winners_week'+id+'_m.png');


              
            
          });


          $('.singleWeek[data-weekid="6"]').trigger('click');
          $('.singleWeek').removeClass('active');
          $('.singleWeek[data-weekid="6"]').addClass('active');



          $('#mapfixer').rwdImageMaps();

          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });

          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          $('.facebookconnect').click(function(e){
            e.preventDefault();
            $('body').addClass("loading");
            window.location='game.php';
          });

          // $('.singleWeek').click(function(e){
          //   e.preventDefault();
            
          // });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });
      });
    </script>


   

  </body>
</html>
    
