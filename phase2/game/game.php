<!doctype html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Win 1 Year Suppply of Pizza!</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">
      <link rel="stylesheet" href="assets/js/fancybox/source/jquery.fancybox.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      

      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.rwdImageMaps.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>
      <script src="assets/js/fancybox/source/jquery.fancybox.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     


      <meta property="og:title" content="Win 1 Year Suppply of Pizza!" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="http://bit.ly/DomiknowItAll" />
      <meta property="og:description" content="Are you a DomiKnow-It-All? Play now to beat my score!" />

      <meta name="twitter:card" value="Are you a DomiKnow-It-All? Play now to beat my score and win 1 year free pizza -> http://bit.ly/DomiKnowItAll">
      
      <style>
          .fancybox-wrap { 
            top: 147px !important; 
          }
          @media all and (max-width: 500px)
          {
              .fancybox-wrap { 
                top: 140px !important; 
              }
          }
      </style>

   </head>

  <body class="loading">
    
    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>

    <div class="desktopContent hidden-xs">
        <div id="game">
          <div class="headBar">
              <div class="left">
                <div class="navbutton">
                  <a class="navicon-button x">
                    <div class="navicon"></div>
                  </a>
                </div>
                <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
              </div>
              <div class="right">
                <div class="level" data-level="0">
                  <img class="playerImage" src="" />
                  <p class="playerName"></p>
                </div>
              </div>
              <img class="logo" src="assets/img/game/logo.png" />
          </div>
          <div class="headBar headBar2">
              <div class="left">
              </div>
              <div class="right">
                <div class="lifes" data-lifes="3">
                  <h1></h1>
                </div>
              </div>
              <div class="timeCounter">
                <h1 class="gameTime">180</h1>
                <h2>seconds</h2>
              </div>
          </div>

          <div class="letters">
              <ul>
                <li data-letter="a" class="activeQuestion">A <img id="letterIndicatorForCurrentQuestion" src="assets/img/game/currentquestion.png" /></li>
                <li data-letter="b">B</li>
                <li data-letter="c">C</li>
                <li data-letter="d">D</li>
                <li data-letter="e">E</li>
                <li data-letter="f">F</li>
                <li data-letter="g">G </li>
                <li data-letter="h">H</li>
                <li data-letter="i">I</li>
                <li data-letter="j">J</li>
                <li data-letter="k">K</li>
                <li data-letter="l">L</li>
                <li data-letter="m">M</li>
                <li data-letter="n">N</li>
                <li data-letter="o">O</li>
                <li data-letter="p">P</li>
                <li data-letter="q">Q</li>
                <li data-letter="r">R</li>
                <li data-letter="s">S</li>
                <li data-letter="t">T</li>
                <li data-letter="u">U</li>
                <li data-letter="v">V</li>
                <li data-letter="w">W</li>
                <li data-letter="x">X</li>
                <li data-letter="y">Y</li>
                <li data-letter="z">Z</li>
              </ul>
          </div>

          <div class="question">
              <h1 class="q">So epic, our <b>Awesome Foursome</b> has Roasted Chicken Drummets, Crazy Chicken Crunchies Original, Crazy Chicken Crunchies Tom Yam and _______</h1>
              <ul class="answer randomShuffler">
                  <li>
                    <img class="hvr-buzz-out answerChoosedByUser" data-answer="1" src="assets/img/game/q/z1img.png" />
                  </li>
                  <li>
                    <img class="hvr-buzz-out answerChoosedByUser" data-answer="2" src="assets/img/game/q/z2img.png" />
                  </li>
                  <li>
                    <img class="hvr-buzz-out answerChoosedByUser" data-answer="3" src="assets/img/game/q/z3img.png" />
                  </li>
                  <li>
                    <img class="hvr-buzz-out answerChoosedByUser" data-answer="4" src="assets/img/game/q/z4img.png" />
                  </li>
              </ul>
          </div>

          <div class="copuns">
            <div class="left">&nbsp;</div>
            <div class="right">
              <img class="timerdesktop" src="assets/img/game/q/timer.png" />
            </div>
            <div class="bottom">&nbsp;</div>
          </div>
         
        </div>
    </div>
































    <div id="mobileGame" class="mobileContent visible-xs">
        <img id="mapfixer" class="img-responsive" src="assets/img/game/mobile/head0.png" usemap="#mobilemap" />
        <map name="mobilemap">
          <area shape="rect" coords="0,21,33,46" class="order" href="menu.php">
        </map>
        <img class="playerImage" />
        <p class="playerName2"></p>
          
       <div class="gameBody">
          
          <div class="left">
              <div class="lifes" data-lifes="3">
                <h1></h1>
              </div>
              <div class="timeCounter">
                <h1 class="gameTime">180</h1>
                <h2>Seconds</h2>
              </div>
              <div class="question">
                <h1 class="letter">A</h1>
                <h3 class="q">So epic, our <b>Awesome Foursome</b> has Roasted Chicken Drummets, Crazy Chicken Crunchies Original, Crazy Chicken Crunchies Tom Yam and _______</h3>
              </div>
              <ul class="answer randomShuffler">
                  <li>
                    <img class="hvr-buzz-out answerChoosedByUser" data-answer="1" src="assets/img/game/q/z1img.png" />
                  </li>
                  <li>
                    <img class="hvr-buzz-out answerChoosedByUser" data-answer="2" src="assets/img/game/q/z2img.png" />
                  </li>
                  <li>
                    <img class="hvr-buzz-out answerChoosedByUser" data-answer="3" src="assets/img/game/q/z3img.png" />
                  </li>
                  <li>
                    <img class="hvr-buzz-out answerChoosedByUser" data-answer="4" src="assets/img/game/q/z4img.png" />
                  </li>
              </ul>
          </div>

          <div class="right">
            <div class="ticker">50</div>
          </div>

       </div>
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 

    </div>

    <!-- <img class="circlelogoutbtn2 btn-menu-mobile visible-xs" src="assets/img/logoutcircle.png" /> -->

      











































































      <!-- popups -->
      <div style="display:none;" id="betweenPopUp">
        <div class="hidden-xs">
          <img class="img-responsive betweenImg hidden-xs" src="assets/img/game/popups/between/g.png" />
          <img id="continue" class="hvr-float closePopup" src="assets/img/game/popups/continue.png" />
        </div>
        <div class="visible-xs">
          <img class="img-responsive betweenImgMobile visible-xs" src="assets/img/game/popups/between/g.png" />
          <img id="continuem" class="hvr-float closePopup" src="assets/img/game/popups/continue.png" />
        </div>
      </div>

      <div style="display:none;" id="sharedalreadypopup">
        <img class="img-responsive hidden-xs" src="assets/img/game/popups/sharedalready.png" />
        <img class="img-responsive visible-xs" src="assets/img/game/popups/sharedalready_mobile.png" />
      </div>


      <div style="display:none;" id="sharepopup">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/share.png" />
          <img id="shareface" class="hvr-float shareface" src="assets/img/game/popups/shareface.png" />
          <img id="sharetwitter" class="hvr-float sharetwitter" src="assets/img/game/popups/sharetwitter.png" />
        </div>
        <div class="visible-xs">
          <img class="img-responsive" src="assets/img/game/popups/share_mobile.png" />
          <img class="shareface centerShare" src="assets/img/game/popups/shareface.png" />
          <img class="sharetwitter centerShare" src="assets/img/game/popups/sharetwitter.png" />
        </div>
      </div>



      <div style="display:none;" id="gameover">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/gameover.png" />
          <img class="hvr-float playagaingame" src="assets/img/game/popups/playagain.png" />
        </div>
        <div class="visible-xs">
           <img class="img-responsive" src="assets/img/game/popups/gameover_mobile.png" />
          <img class="hvr-float playagaingame" src="assets/img/game/popups/playagain.png" />
        </div>
      </div>




      <div style="display:none;" id="nomorelifes">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/nomorepizza.png" />
          <img id="shareface" class="hvr-float shareface" src="assets/img/game/popups/shareface.png" />
          <img id="sharetwitter" class="hvr-float sharetwitter" src="assets/img/game/popups/sharetwitter.png" />
        </div>
        <div class="visible-xs">
          <img class="img-responsive" src="assets/img/game/popups/nomorepizza_mobile.png" />
          <img class="shareface centerShare" src="assets/img/game/popups/shareface.png" />
          <img class="sharetwitter centerShare" src="assets/img/game/popups/sharetwitter.png" />
        </div>
      </div>


      <!-- popups end -->












































































    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});

      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };

      //facebook api
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1487607144892582',
          xfbml      : true,
          version    : 'v2.4'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));


      //my awesome jQuery ranomize plugin
      (function($) {
        $.fn.randomize = function(childElem) {
          return this.each(function() {
              var $this = $(this);
              var elems = $this.children(childElem);
              elems.sort(function() { return (Math.round(Math.random())-0.5); });  
              $this.remove(childElem);  
              for(var i=0; i < elems.length; i++)
                $this.append(elems[i]);      
          });    
        }
      })(jQuery);


      // global variables
      var user=sessionStorage.getObj('tjuser');
      var gameTime = 180;
      var wrongAnswersLeft=3;
      var userlifes=0;
      var hasSharedFacebookToday=false;
      var hasSharedTwitterToday=false;
      var tickInterval;
      var shareLink = 'http://bit.ly/DomiknowItAll';
      var shareTitlef = 'Are you a DomiKnow-It-All? Play now to beat my score!';
      var shareTitlet = 'Are you a DomiKnow-It-All? Play now to beat my score and win 3 years free pizza!';
      var ajaxLocked = false;
      var lifeReduced = false;
      var previousLetter = 'a';
      
      var letters= ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
      var currentQuestion=0;
      var questions= [];
      questions['a']= {q:'So epic, our <b>Awesome Foursome</b> has Roasted Chicken Drummets, Crazy Chicken Crunchies Original, Crazy Chicken Crunchies Tom Yam and _______'};
      questions['b']= {q:'Domino\'s has 3 yummy flavors <b>Cheese Burst Crusts</b>, cheesy original, spicy chilli and smoky_______'};
      questions['c']= {q:'Our Promotions section has three options for you to choose from. Highlights, ________ & Express Card.'};
      questions['d']= {q:'Domino\'s online account holders get to register up ______ addresses.'};
      questions['e']= {q:'If you had a Domino\'s <b>Express Card</b> you\'d get _______ all year!'};
      questions['f']= {q:'We have FIVE pizza sauce FLAVORS, which of these is not one of them?'};
      questions['g']= {q:'What does our <b>GPS tracker</b> stand for?'};
      questions['h']= {q:'You want Aloha Chicken and your friend wants BBQ Chicken. How can you satisfy both with one pizza?'};
      questions['i']= {q:'S.H.I.E.L.D has the Incredible Hulk, Domino\'s has________'};
      questions['j']= {q:'Can you guess how many Domino\'s Stores there are in <b>Johor</b>?'};
      questions['k']= {q:'Do you know how many stores we have in <b>Kelantan</b>?'};
      questions['l']= {q:'Think you know which of these is a part of our menu?'};
      questions['m']= {q:'Which one of these is our pizza and not a dinosaur!'};
      questions['n']= {q:'Bonus round! Our <b>New Website</b> is convenient to order online because caaaaaan…..'};
      questions['o']= {q:'When you <b>order online</b>, you can make a timed order of up to ___ days in advance.'};
      questions['p']= {q:'What type of Domino’s Party could you win invites to?'};
      questions['q']= {q:'Which of these are not in our list of <b>Quality</b> Ingredients? '};
      questions['r']= {q:'Which of these pizzas have this symbol on it? (<img style="width:50px;" src="assets/img/game/q/Chili_QuestionR.png" />) '};
      questions['s']= {q:'Plenty of Side Orders for you to pick but which of these are packed for sharing? '};
      questions['t']= {q:'Can you keep a secret? We\'ve slipped our <b>Top Secret sauces</b> into one of our pizzas but which one was it?  '};
      questions['u']= {q:'Which of these ingredients is NOT in our <b>Ultimate Hawaiian</b> pizza?'};
      questions['v']= {q:'A vegetarians favourite pizza is….'};
      questions['w']= {q:'Our <b>wings</b> come in 3 flavours. Plain, BBQ and _____'};
      questions['x']= {q:'What\'s the biggest size of pizza we serve? '};
      questions['y']= {q:'Whats another name for <b>Yankee crust</b>?'};
      questions['z']= {q:'How much does Domino\'s charge for delivery?'};

      //prevent cheating
      if(user == null)
      {
        window.location="menu.php";
        user = '-1';
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        //logoutFromTheGame();
      }
     
      // load user info
      $('.playerImage').attr('src',user.picture);
      $('.playerName').html(user.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
      $('.playerName2').html(user.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');

      function logoutFromTheGame()
      {
        user = '-1';
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#game').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#gamemobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }

      function gameOver()
      {
        console.log('game over function not FULLY implemented yet .. its only reduce the lifes and stops the time now !');
        // clear timer now
        clearInterval(tickInterval);
        // decrease the lifes by one;
        $.post('engine/userDie.php',{u:user.email},function(data){},"json");
      }

      function goToQuestion(n)
      {
        //changing the question and answers

        $('.question .q').html(questions[n].q);
        $('.question .letter').html(n);

        $('.letters ul li').removeClass('activeQuestion');
        $('.letters ul li[data-letter="'+n+'"]').addClass('activeQuestion');
        $('#letterIndicatorForCurrentQuestion').remove();
        $('.letters ul li[data-letter="'+n+'"]').append(' <img id="letterIndicatorForCurrentQuestion" src="assets/img/game/currentquestion.png" />');
        $('.answerChoosedByUser[data-answer="1"]').attr('src','assets/img/game/q/'+n+'1img.png');
        $('.answerChoosedByUser[data-answer="2"]').attr('src','assets/img/game/q/'+n+'2img.png');
        $('.answerChoosedByUser[data-answer="3"]').attr('src','assets/img/game/q/'+n+'3img.png');
        $('.answerChoosedByUser[data-answer="4"]').attr('src','assets/img/game/q/'+n+'4img.png');

        //shuffling the answers randomly
        $(".randomShuffler").randomize(".randomShuffler li");
        


        if(previousLetter=='s' || previousLetter == 'o' || previousLetter == 'j' || previousLetter == 'i' || previousLetter == 'g' ||  previousLetter == 'e' || previousLetter == 'd' || previousLetter == 'c')
        {
            $('.betweenImg').attr('src','assets/img/game/popups/between/'+previousLetter+'.png');
            $('.betweenImgMobile').attr('src','assets/img/game/popups/between/'+previousLetter+'mobile.png');
            var lettertobechanged = previousLetter;
            //used the timeout to fix the issue of showing the popup before changine the image
            setTimeout(function(){
              $('.betweenImg').attr('src','assets/img/game/popups/between/'+lettertobechanged+'.png');
              $('.betweenImgMobile').attr('src','assets/img/game/popups/between/'+lettertobechanged+'mobile.png');
              clearInterval(tickInterval);
              $.fancybox.open($("#betweenPopUp"),
              {
                  topRatio:0.5,
                  maxWidth: 900,
                  width: 900,
                  'afterClose' : function () { 
                    tickInterval = setInterval(tick,1000);
                  },
                  closeBtn : false,
                  helpers : {
                    overlay : {
                      css : {
                        'background' : 'rgba(0, 0, 0, 0.3)'
                      }
                    }
                  }  
              });
            },300);
        }

        if(previousLetter=='i')
        {
          //level 10
          $('#mapfixer').attr('src','assets/img/game/mobile/head10.png');
          $('.level').attr('data-level','10');
        }
        if(previousLetter=='s')
        {
          //level 20
          $('#mapfixer').attr('src','assets/img/game/mobile/head20.png');
          $('.level').attr('data-level','20');
        }
        if(previousLetter=='y')
        {
          //level 26
          $('#mapfixer').attr('src','assets/img/game/mobile/head26.png');
          $('.level').attr('data-level','26');
        }

        previousLetter = n;
      }

      function userWin()
      {
        console.log('user have won take him results page');
        //CLEAR INTERVAL
        clearInterval(tickInterval);
        $('body').addClass('loading');
        //store my time in server session variable
        //alert(user.dominosEmail);
        $.post('engine/setWinnerTime.php',{'t':gameTime,'email':user.email, 'demail':user.dominosEmail},function(data){
          //and then redirect me to results page
          $('body').removeClass('loading');
          window.location='results.php';
        },"json");

        
        
      }

      function tick()
      {
        gameTime--;

        /* desktop tick */
        var barWidth = $('.copuns .right').width();
        var step = barWidth / 182; // time plus 2
        var current = $('.timerdesktop').position().left;
        var next = current+step;
        $('.timerdesktop').css('left',next+'px');
        $('.timerdesktop').attr('data-tooltip','You can do it');
        $('.gameTime').html(gameTime);

        /* mobile tick */
        var barWidthmobile = $('#mobileGame .right').height();
        var stepmobile = barWidthmobile / 182; // time plus 2
        var currentmobile = $('#mobileGame .right .ticker').position().top;
        var nextmobile = currentmobile+stepmobile;
        $('#mobileGame .right .ticker').css('top',nextmobile+'px');
        $('#mobileGame .right .ticker').attr('data-tooltip','You can do it');
        $('.gameTime').html(gameTime);
        if(gameTime >= 150)
        {
          $('#mobileGame .right .ticker').html('50');
        }
        else if(gameTime >= 120 && gameTime < 150)
        {
          $('#mobileGame .right .ticker').html('40');
        }
        else if(gameTime >= 60 && gameTime < 120)
        {
          $('#mobileGame .right .ticker').html('20');
        }
        else if(gameTime >= 0 && gameTime < 60)
        {
          $('#mobileGame .right .ticker').html('10');
        }


        if(gameTime <= 0)
        {
          /* game over desktop */
          $('.timerdesktop').css('left',(barWidth-12)+'px');
          $('.timerdesktop').attr('data-tooltip','You are looser');
          /* game over mobile */
          $('#mobileGame .right .ticker').css('top',(barWidthmobile-12)+'px');
          $('#mobileGame .right .ticker').attr('data-tooltip','You are looser');
          
          window.location='timeout.php';
         
          gameOver();
          return;
        }


        if(gameTime <= 174)
        {
          if(!lifeReduced)
          {
            lifeReduced=true;
            // decrease the lifes by one; so if he refreshed he already lose the life !
            // $.post('engine/userDie.php',{u:user.email},function(data){},"json");
          }
        }

      }
      
      
      $(function(){



          //menu
          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });
          //image map
          $('#mapfixer').rwdImageMaps();


          //initializeing the game
          $.post('engine/init.php',{'u':user.email},function(data){
            $('body').removeClass('loading');
            userlifes = data.lifes;
            if(userlifes <= 0)
            {
              $('.lifes h1').html('0');
              $('.lifes').attr('data-lifes',0);
              //alert('no more lifes');
              $.fancybox.open($("#nomorelifes"),
              {
                  topRatio:0.5,
                  maxWidth: 900,
                  width: 900,
                  'afterClose' : function () { 
                   
                  },
                  closeClick  : false,
                  closeBtn : true,
                  helpers : {
                    overlay : {
                      css : {
                        'background' : 'rgba(0, 0, 0, 0.3)'
                      },
                      closeClick: false
                    }
                  },
                  afterClose: function(){
                    window.location='menu.php';
                  }  
              });
              return;
            }
            $('.lifes h1').html(userlifes);

            // increase the counter of first game by one;
            $.post('engine/onemoregame.php',{u:user.email},function(data){},"json");

            goToQuestion(letters[currentQuestion]);
            //goToQuestion('r');
            tickInterval = setInterval(tick,1000);
          },"json");


          //choose the answer
          $('.answerChoosedByUser').click(function(e){
            if(ajaxLocked)
              return;

            e.stopPropagation();
            var a = $(this).data('answer');
            var answer = $(this).parent();
            //check if the answer is correct
            ajaxLocked=true;
            $.post("engine/check_answer.php",{'q':letters[currentQuestion],'a':a},function(data){
              if(data.status=='wrong')
              {
                //wrong answer
                answer.addClass('wrong');
                wrongAnswersLeft--;
                if(wrongAnswersLeft > -1)
                {
                  $('.lifes').attr('data-lifes',wrongAnswersLeft);
                }
                setTimeout(function(){
                   ajaxLocked=false;
                  $('.answer li').removeClass('wrong');
                  if(wrongAnswersLeft <=0)
                  {

                      $.fancybox.open($("#gameover"),
                      {
                          topRatio:0.5,
                          maxWidth: 900,
                          width: 900,
                          'afterClose' : function () { 
                           
                          },
                          helpers : {
                            overlay : {
                              css : {
                                'background' : 'rgba(0, 0, 0, 0.3)'
                              }
                            }
                          },
                          afterClose: function(){
                            window.location='game.php';
                          }  
                      });

                      gameOver();
                  }
                },1000);
              }
              else
              {
                 ajaxLocked=false;
                //correct answer
                if(letters[currentQuestion] == 'z')
                {
                  userWin();

                }
                else
                {
                  currentQuestion++;
                  goToQuestion(letters[currentQuestion]);
                }
              }
            },"json");
          });

          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          $('.shareface').click(function(e){
              e.preventDefault();
              $('body').addClass("loading");
              FB.ui(
                {
                  method: 'share',
                  href: shareLink
                },
                // callback
                function(response) {
                   $('body').removeClass("loading");
                  if (response && !response.error_message) {

                    // add one more live because of sharing
                    $.post('engine/userSharedFacebook.php',{u:user.email},function(data){},"json");
                    window.location='game.php';

                  } else {

                    noty({
                      type:'warning',
                      text:'Your post hasn\'t been shared. Please <b>Share it</b> to get more lifes.',
                      timeout: 7500,
                      layout:'topCenter',
                       animation: {
                          open: 'animated bounceInLeft',
                          close: 'animated zoomOutLeft',
                          easing: 'swing',
                          speed: 500
                      }
                    });
                    return;

                  }
                }
              );
          });

          $('.sharetwitter').click(function(e){
            e.preventDefault();
            //alert('share twitter not implemented yet');
            $('body').addClass("loading");
            var win = window.open("https://twitter.com/intent/tweet?text="+encodeURIComponent(shareTitlet)+"&url="+ encodeURIComponent(shareLink)+"");
            var timer = setInterval(function() {   
                if(win.closed) {  
                    clearInterval(timer);  
                    // add one more live because of sharing
                    $.post('engine/userSharedTwitter.php',{u:user.email},function(data){},"json");
                    window.location='game.php';
                }  
            }, 1000);
          });

          $('.closePopup').click(function(e){
            e.preventDefault();
            //alert('play again clicked');
            $.fancybox.close();
          });

          $('.playagaingame').click(function(e){
            e.preventDefault();
            //alert('play again clicked');
            window.location='game.php';
          });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });


          // $.fancybox.open($("#nomrelifes"),
          //     {
          //         topRatio:0.5,
          //         maxWidth: 900,
          //         width: 900,
          //         'afterClose' : function () { 
                   
          //         },
          //         helpers : {
          //           overlay : {
          //             css : {
          //               'background' : 'rgba(0, 0, 0, 0.3)'
          //             }
          //           }
          //         }  
          // });

          
          
      });
    </script>


   

  </body>
</html>
    
