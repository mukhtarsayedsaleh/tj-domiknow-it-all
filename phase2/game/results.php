<?php 
    session_start();
    if(!isset($_SESSION["win"]) || !isset($_SESSION["time"]))
    {
      //die('Hello bro,<br/><b>cheating</b> is not good :)');
      header('location: menu.php');
      return;
    }

    //calculate the game points from remaining game time (time * 2)
    $gs = $_SESSION["time"] * 2;

    //store the score of the game in the database
    include 'engine/config.php';
      //get id of this user
      $user = ORM::for_table('user')->where(array('email'=>$_SESSION['email']))->find_one();
      $userid = $user->id;
      //save game points of this user
      $score = ORM::for_table('user_score')->create();
      $score->user_id = $userid;
      $score->score = $gs;
      $score->score_source = 'game';
      $score->score_date = date('Y-m-d H:i:s', strtotime('+8 hours'));
      try
      {
        $score->save();
      }
      catch(Exception $e){
        echo $e;
      }



    //share points
      //get current week begin and end
      $week = ORM::for_table(null)->raw_query('SELECT `weekname` , date(start) as `start`, date(end) as `end` FROM week where now() >= start and now() <= end')->find_one();
      $thisweekstart = $week['start'];
      $thisweekend = $week['end'];

      //get share pointes from our database
      //get lifes of this user during this week of campaign
      $ss =ORM::for_table(null)
                ->raw_query('select ifnull(sum(T.score),0) as `score` from (SELECT `score`,`score_source`,Date(`score_date`)
                                                                  FROM user_score where
                                                                  user_id = '.$userid.' 
                                                                  and (`score_source` = \'Share twitter\' or `score_source` = \'Share facebook\' )
                                                                  and (`score_date` between \''.$thisweekstart.' 00:00:00\' and \''.$thisweekend.' 23:59:59\') group by score_source,DATE(`score_date`)) T')
                ->find_one()
                ['score'];

    //multiplier
    // if no of pizza during this week is 2+ we will multuply.
    // else multiplier is 1 by default.
    $multiplier = 1;
      // OK cool - then let's create a new cURL resource handle
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://api.dominos.com.my/api/GameContest/GetCustomerCheeseBurstInfo');
      curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
      curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch,CURLOPT_POST,true);
   
      $data = array('Email' => $_SESSION['demail'], 
                    'AppKey' => 'DominosFBGame',
                    'StartDate' => $thisweekstart.'', 
                    'EndDate' => $thisweekend.'');
      //var_dump($data); 
                                                                       
      $data_string = json_encode($data);    
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
          'Content-Type: application/json',                                                                                
          'Content-Length: ' . strlen($data_string))                                                                       
      );

      if( ! $output = curl_exec($ch)) 
      { 
          echo curl_error($ch); 
      }
      // test
      // $largePizzas = 4;
      // $regularPizzas = 3;
      
      $largePizzas = json_decode($output)->CheeseBurstLargeTotalCount;
      $regularPizzas = json_decode($output)->CheeseBurstRegularTotalCount;
      $pizzas = (2 * $regularPizzas) + (3 * $largePizzas);
      if($pizzas >= 2)
      {
        $multiplier=$pizzas;
      }
      curl_close($ch);


    // final result is
    $final = ( $gs + $ss ) * $multiplier;


    //get gift for user
    if($_SESSION["time"] >= 0 && $_SESSION["time"] < 30)
    {
      $gift = 'WCB1'; //30 -0
    }
    if($_SESSION["time"] >= 30 && $_SESSION["time"] < 60)
    {
      $gift = 'WCB2'; // 60 - 30
    }
    if($_SESSION["time"] >= 60 && $_SESSION["time"] < 90)
    {
      $gift = 'WCB3'; // 90 - 60
    }
    if($_SESSION["time"] >= 90 && $_SESSION["time"] < 120)
    {
      $gift = 'WCB4'; // 120 - 90 
    }
    if($_SESSION["time"] >= 120 && $_SESSION["time"] < 150)
    {
      $gift = 'WCB5'; // 150 - 120
    }
    if($_SESSION["time"] >= 150 && $_SESSION["time"] < 180)
    {
      $gift = 'WCB6'; // 180 - 150
    }
    $giftimg = 'assets/img/game/results/'.$gift.'.png';

    //var_dump($gift);

    // send the gift for this user to dominos site
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.dominos.com.my/api/GameContest/AwardFBCoupon');
    curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch,CURLOPT_POST,true);
 
    $data = array('Email' => $_SESSION['demail'], 
                  'AppKey' => 'DominosFBGame',
                  'FBCouponCode' => $gift);                                               
    $data_string = json_encode($data);    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen($data_string))                                                                       
    );
    if( ! $output = curl_exec($ch)) 
    { 
        echo curl_error($ch); 
    }
    // var_dump($_SESSION['demail']);
    // var_dump($gift);
    //var_dump($output);

    curl_close($ch);


    // store user as winner
      //get id of this user
      $user = ORM::for_table('user')->where(array('email'=>$_SESSION['email']))->find_one();
      $userid = $user->id;
      //reduce lifes of this user
      $winner = ORM::for_table('gifts')->create();
      $winner->user_id = $userid;
      $winner->gamedatetime = date("Y:m:d H:i:s", strtotime('+8 hours'));
      $winner->copun = $gift;
      try
      {
        $winner->save();
      }
      catch(Exception $e){
        echo $e;
      }

    //clear session variables in order not to let user add score more than one time ! :)
    unset($_SESSION["win"]);


?>
<!doctype html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Win 1 Year Suppply of Pizza!</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">
      <link rel="stylesheet" href="assets/js/fancybox/source/jquery.fancybox.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      

      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.rwdImageMaps.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>
      <script src="assets/js/fancybox/source/jquery.fancybox.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     


    <meta property="og:title" content="Win 1 Year Suppply of Pizza!" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://bit.ly/DomiknowItAll" />
    <meta property="og:description" content="Are you a DomiKnow-It-All? Play now to beat my score!" />

    <meta name="twitter:card" value="Are you a DomiKnow-It-All? Play now to beat my score and win 1 year free pizza -> http://bit.ly/DomiKnowItAll">
     



   </head>

  <body>
    
    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>
    
    <div class="desktopContent hidden-xs">
        <div id="game">
          <div class="headBar">
              <div class="left">
                <div class="navbutton">
                  <a class="navicon-button x">
                    <div class="navicon"></div>
                  </a>
                </div>
                <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
              </div>
              <div class="right">
                <div class="level" data-level="0">
                  <img class="playerImage" src="" />
                  <p class="playerName"></p>
                </div>
              </div>
              <img class="logo" src="assets/img/game/logo.png" />
          </div>
          

          

          <div class="resultsdivdesktop">
            <h1 class="gameScore"><?php echo $gs; ?></h1>
            <h1 class="shareScore"><?php echo $ss; ?></h1>
            <h1 class="multiplierScore"><small>x</small><?php echo $multiplier; ?></h1>
            <h1 class="totalScore"><?php echo $final; ?></h1>
            <img class="copun" src="<?php echo $giftimg; ?>" />
            <img class="shareface hvr-float" src="assets/img/game/results/facebook.png" />
            <img class="sharetwitter hvr-float" src="assets/img/game/results/twitter.png" />
            <img class="orderbtnresults hvr-float img-responsive" src="assets/img/game/results/orderbtn.png" />
            <img class="playagain hvr-float img-responsive" src="assets/img/game/results/playagain.png" />
          </div>
          
          <!-- <img class="hvr-float playagaingame" style="margin-top:-70px !important;" src="assets/img/game/popups/playagain.png" />
          -->
        </div>
    </div>
































    <div id="mobileGame" class="mobileContent visible-xs">
        <img id="mapfixer" class="img-responsive" src="assets/img/game/mobile/head0.png" usemap="#mobilemap" />
        <map name="mobilemap">
          <area shape="rect" coords="0,21,33,46" class="order" href="menu.php">
        </map>
        <img class="playerImage" />
        <p class="playerName2"></p>
          
        <div class="resultsdivmobile">
          <h1 class="gameScore"><?php echo $gs; ?></h1>
          <h1 class="shareScore"><?php echo $ss; ?></h1>
          <h1 class="multiplierScore"><small>x</small><?php echo $multiplier; ?></h1>
          <h1 class="totalScore"><?php echo $final; ?></h1>
          <img class="copun" src="<?php echo $giftimg; ?>" />
          <img class="shareface " src="assets/img/game/results/facebook.png" />
          <img class="sharetwitter " src="assets/img/game/results/twitter.png" />
        </div>
        
        <img class="orderbtnresults img-responsive" src="assets/img/game/results/orderbtn.png" />
        <img class="playagain img-responsive" src="assets/img/game/results/playagain.png" />

        <div>&nbsp;</div> 
        <div>&nbsp;</div> 
        <div>&nbsp;</div> 
        <div>&nbsp;</div> 

    </div>

    <!-- <img class="circlelogoutbtn2 btn-menu-mobile visible-xs" src="assets/img/logoutcircle.png" /> -->

      











































































      <!-- popups -->
      <div style="display:none;" id="sharedalreadypopup">
        <img class="img-responsive hidden-xs" src="assets/img/game/popups/sharedalready.png" />
        <img class="img-responsive visible-xs" src="assets/img/game/popups/sharedalready_mobile.png" />
      </div>


      <div style="display:none;" id="sharepopup">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/share.png" />
          <img id="shareface" class="hvr-float shareface" src="assets/img/game/popups/shareface.png" />
          <img id="sharetwitter" class="hvr-float sharetwitter" src="assets/img/game/popups/sharetwitter.png" />
        </div>
        <div class="visible-xs">
          <img class="img-responsive" src="assets/img/game/popups/share_mobile.png" />
          <img class="shareface centerShare" src="assets/img/game/popups/shareface.png" />
          <img class="sharetwitter centerShare" src="assets/img/game/popups/sharetwitter.png" />
        </div>
      </div>



      <div style="display:none;" id="gameover">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/gameover.png" />
          <img class="hvr-float playagaingame" src="assets/img/game/popups/playagain.png" />
        </div>
        <div class="visible-xs">
           <img class="img-responsive" src="assets/img/game/popups/gameover_mobile.png" />
          <img class="hvr-float playagaingame" src="assets/img/game/popups/playagain.png" />
        </div>
      </div>




      <div style="display:none;" id="nomrelifes">
        <div class="hidden-xs">
          <img class="img-responsive" src="assets/img/game/popups/nomorepizza.png" />
          <img id="shareface" class="hvr-float shareface" src="assets/img/game/popups/shareface.png" />
          <img id="sharetwitter" class="hvr-float sharetwitter" src="assets/img/game/popups/sharetwitter.png" />
        </div>
        <div class="visible-xs">
          <img class="img-responsive" src="assets/img/game/popups/nomorepizza_mobile.png" />
          <img class="shareface centerShare" src="assets/img/game/popups/shareface.png" />
          <img class="sharetwitter centerShare" src="assets/img/game/popups/sharetwitter.png" />
        </div>
      </div>


      <!-- popups end -->












































































    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});

      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };

      //facebook api
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1487607144892582',
          xfbml      : true,
          version    : 'v2.4'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));

      // global variables
      var shareLink = 'http://bit.ly/DomiknowItAll';
      var shareTitlef = 'Are you a DomiKnow-It-All? Play now to beat my score!';
      var shareTitlet = 'Are you a DomiKnow-It-All? Play now to beat my score and win 3 years free pizza!';

      //prevent cheating
      var user=sessionStorage.getObj('tjuser');
      if(user == null)
      {
        logoutFromTheGame();
      }
     
      // load user info
      $('.playerImage').attr('src',user.picture);
      $('.playerName').html(user.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
      $('.playerName2').html(user.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');

      function logoutFromTheGame()
      {
        user = '-1';
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#game').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#gamemobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }


    
      
      
      $(function(){

         
          $('#mapfixer').rwdImageMaps();

          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });

          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          $('.shareface').click(function(e){
            e.preventDefault();
              $('body').addClass("loading");
              FB.ui(
                {
                  method: 'share',
                  href: shareLink
                },
                // callback
                function(response) {
                   $('body').removeClass("loading");
                  if (response && !response.error_message) {

                    // add one more live because of sharing
                    $.post('engine/userSharedFacebook.php',{u:user.email},function(data){},"json");
                    //window.location='game.php';
                    noty({
                      type:'success',
                      text:'Successfully shared! The points and 1 live will be added to your account in the next round',
                      timeout: 7500,
                      layout:'center',
                       animation: {
                          open: 'animated bounceInLeft',
                          close: 'animated zoomOutLeft',
                          easing: 'swing',
                          speed: 500
                      }
                    });
                    return;

                  } else {

                    noty({
                      type:'warning',
                      text:'Your post hasn\'t been shared. Please <b>Share it</b> to get more lifes.',
                      timeout: 7500,
                      layout:'topCenter',
                       animation: {
                          open: 'animated bounceInLeft',
                          close: 'animated zoomOutLeft',
                          easing: 'swing',
                          speed: 500
                      }
                    });
                    return;

                  }
                }
              );
          });

          $('.sharetwitter').click(function(e){
            e.preventDefault();
            //alert('share twitter not implemented yet');
            $('body').addClass("loading");
            var win = window.open("https://twitter.com/intent/tweet?text="+encodeURIComponent(shareTitlet)+"&url="+ encodeURIComponent(shareLink)+"");
            var timer = setInterval(function() {   
                if(win.closed) {  
                    clearInterval(timer);  
                    // add one more live because of sharing
                    $.post('engine/userSharedTwitter.php',{u:user.email},function(data){},"json");
                    //window.location='game.php';
                    $('body').removeClass("loading");
                    noty({
                      type:'success',
                      text:'Successfully twitted! The points and 1 live will be added to your account in the next round',
                      timeout: 7500,
                      layout:'center',
                       animation: {
                          open: 'animated bounceInLeft',
                          close: 'animated zoomOutLeft',
                          easing: 'swing',
                          speed: 500
                      }
                    });
                    return;

                }  
            }, 1000);
          });

          $('.playagain').click(function(e){
            e.preventDefault();
            window.location = 'game.php';
          });

          $('.orderbtnresults').click(function(e){
            e.preventDefault();
            window.top.location.href = 'https://www.dominos.com.my/meals?utm_source=Game&utm_medium=Game%20Over&utm_campaign=DomiKnowItAll';
          });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });


          // $.fancybox.open($("#nomrelifes"),
          //     {
          //         topRatio:0.5,
          //         maxWidth: 900,
          //         width: 900,
          //         'afterClose' : function () { 
                   
          //         },
          //         helpers : {
          //           overlay : {
          //             css : {
          //               'background' : 'rgba(0, 0, 0, 0.3)'
          //             }
          //           }
          //         }  
          // });
          
      });
    </script>

  

  </body>
</html>