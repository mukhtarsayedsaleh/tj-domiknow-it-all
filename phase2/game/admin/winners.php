<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Domino's - MegaRaya Winners</title>	
	<?php include 'head.html'; ?>
</head>
<body>
	<div class="container-fluid">
		<div id="wrapper">		
			<?php include 'menu.php'; ?>
			<div id="content">
				<h3>List of Winners</h3>
				<div class="inner">
					<div>
						<table class="data table table-striped table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Played</th>
									<th>Score</th>
									<th>Date of Play</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td class="center">-</td>
									<td class="center">-</td>
									<td class="center">-</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>	
	<script>
		$(function(){			
			$("#menu div ul li").addClass(function(index, currentClass) {
				var addedClass;
				if ( currentClass === "menu2" ) {
				  addedClass = "active";      
				}			  
				return addedClass;
			 });
		});
	</script>
</body>
</html>