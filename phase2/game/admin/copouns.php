<?php 
	error_reporting(E_ALL); 
	ini_set('display_errors', 1);
	session_start();
	if($_SESSION['user'] != true)
	{
		echo "<script>document.location.href='login.php';</script>";
	}

	include '../engine/config.php';

	
	$sql = "SELECT gifts.`gamedatetime`,gifts.`copun`,(select name from user where user.id=gifts.`user_id`) as `name` FROM gifts";
	$users = ORM::for_table(null)->raw_query($sql)->find_many();
	
?>


<!DOCTYPE html>
	<head>
		<meta charset="utf-8">
		<title>Domino's admin dashboard</title>
		<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
		<link type='text/css' rel='stylesheet' href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
		<script type="text/javascript">
			$(document).ready(function() {
			    $('#example').DataTable();
			});
		</script>

		<style type="text/css">
			td,th
			{
				text-align: center;	
			}
		</style>
	</head>
	<body>
		<center><h1>All copouns</h1></center>
		<table id="example" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>user name</th>
					<th>Copoun</th>
					<th>Date and time</th>
				</tr>
			</thead>

			<tbody>
				<?php 
					foreach($users as $u)
					{

						echo '<tr>
									<td>'.$u['name'].'</td>
									<td>'.$u['copun'].'</td>
									<td>'.$u['gamedatetime'].'</td>
								</tr>';
					}
				?>
			</tbody>
		</table>
	</body>
</html>