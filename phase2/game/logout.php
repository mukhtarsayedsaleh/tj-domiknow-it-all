<!doctype html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Domino's A-Z Game</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      


      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.rwdImageMaps.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     

      <style type="text/css">
        .orderNowlOGOUT
        {
          cursor: pointer;
          margin: 20px auto;
          display: block;
        }
      </style>

     

   </head>

  <body>
    
    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>
    
    <div class="desktopContent hidden-xs">
        <div id="step2">
          <div id="game">
            <div class="headBar">
                <div class="left">
                  <div class="navbutton">
                    <a class="navicon-button x">
                      <div class="navicon"></div>
                    </a>
                  </div>
                  <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
                </div>
                <div class="right">
                <!-- <img class="playnowsmall hvr-float" src="assets/img/menu/playsmall.png" /> -->
                </div>
                <img class="logo" src="assets/img/game/logo.png" />
            </div>
            <script type="text/javascript">
            $(function(){
              $('.playnowsmall').click(function(){
                window.location="game.php";
              });
            });
            </script>
          </div>
          <br/>
          <img class="logo img-responsive" src="assets/img/game/pages/logout.png" />
          <img class="hvr-float orderNowlOGOUT" src="assets/img/game/pages/ordernowdesktop.png" />
          <img class="logo img-responsive" src="assets/img/game/pages/logout2.png" />
        </div>
    </div>
































    <div id="mobileGame" class="mobileContent visible-xs">
        <img id="mapfixer" class="img-responsive" src="assets/img/game/mobile/head-1.png" usemap="#mobilemap" />
        <map name="mobilemap">
          <area shape="rect" coords="0,21,33,46" class="order" href="menu.php">
        </map>
        <img class="playerImage" />
        <p class="playerName2"></p>
        
        <img class="logo img-responsive" src="assets/img/game/pages/logoutmobile.png" />
        <img class="img-responsive orderNowlOGOUT" style="max-width:200px;" src="assets/img/game/pages/ordernowdesktop.png" />
        <img class="logo img-responsive" src="assets/img/game/pages/logout2mobile.png" />

      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 

    </div>

    <!-- <img class="circlelogoutbtn btn-menu-mobile visible-xs" src="assets/img/logoutcircle.png" /> -->

      
    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});
    </script>

    <script type="text/javascript">
      //facebook api
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1487607144892582',
          xfbml      : true,
          version    : 'v2.4'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));



      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };

      

      function validateEmail(email) {
          var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
          return re.test(email);
      }

      // global variables
      var user=sessionStorage.getObj('tjuser');

      // //prevent cheating
      // if(user == null)
      // {
      //   logoutFromTheGame();
      // }
     
      // load user info
      if(user != null)
      {
        $('.playerImage').attr('src',user.picture);
        $('.playerName').html(user.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
        $('.playerName2').html(user.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');
      }

      function logoutFromTheGame()
      {
        user = '-1';
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#step2').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#step2mobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }

      
      
      $(function(){

          try
          {
            FB.logout();sessionStorage.setObj('tjuser',user);
          }catch(e){}

          $('#mapfixer').rwdImageMaps();

          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });

          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          $('.orderNowlOGOUT').click(function(e){
            e.preventDefault();
            window.top.location.href = 'https://www.dominos.com.my/meals?utm_source=Game&utm_medium=Log%20Out&utm_campaign=DomiKnowItAll';
          });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });
      });
    </script>


  </body>
</html>
    
