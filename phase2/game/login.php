<!doctype html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Domino's A-Z Game</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      


      <script src="assets/js/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     

      <style type="text/css">
          .textloginmobile,.loginbtnformobile
          {
            float: left;
          }
          .textloginmobile
          {
            margin-left: 5% !important;
          }
          .loginbtnformobile
          {
            width: 32px;
            margin-top: 3px;
            margin-left: 3px;
          }
          #dominosEmailmobileErrorMessage
          {
            clear: both;
          }
      </style>
     

   </head>

  <body>
    
    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>
    
    <div class="desktopContent hidden-xs">
        <div id="step1">
          <img class="logo" src="assets/img/login/logo_desktop.png" />
          <img class="step1instruction" src="assets/img/login/step1instruction.png" />
          <img id="facebookConnectDesktop" class="facebookconnect hvr-float" src="assets/img/login/facebook_connect_desktop.png" />
        </div>

        <div style="display:none;" id="step2">
          <div class="headBar">
              <div class="left">
                <div class="navbutton">
                  <a class="navicon-button x">
                    <div class="navicon"></div>
                  </a>
                </div>
                <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
              </div>

              <div class="right">
                <div class="level" data-level="0">
                  <img class="playerImage" src="" />
                  <p class="playerName"></p>
                </div>
              </div>
          </div>
          <img class="logo" src="assets/img/login/logo_desktop.png" />
          <img class="step2instruction" src="assets/img/login/step2instruction.png" />
          <input type="text" id="dominosEmail" />
          <p id="dominosEmailErrorMessage" style="display:none;" class="errorMessage">Please enter the correct Domino's email. Or register</p>
          <p class="donthaveaccount">DON'T HAVE ONE?&nbsp;&nbsp;&nbsp;<a class="registerlink">REGISTER NOW</a></p>
        </div>
    </div>
































    <div class="mobileContent visible-xs">
       <div id="step1mobile">
          <img class="logo img-responsive" src="assets/img/login/logo_mobile.png" />
          <img id="facebookConnectMobile" class="facebookconnect img-responsive" src="assets/img/login/facebook_connect_mobile.png" />
        </div>

        <div style="display:none;" id="step2mobile">
          <div class="headBar">
              <div class="right">
                <div class="level" data-level="0">
                  <img class="playerImage" src="" />
                </div>
                <br/><br/><br/>
                <p class="playerName2"></p>
              </div>
          </div>
          <img class="logo img-responsive" src="assets/img/login/logo_mobile2.png" />
          <input class="textloginmobile" type="text" id="dominosEmailmobile" />
          <img class="loginbtnformobile" src="assets/img/login/loginbtnmobile.png" />
          <p id="dominosEmailmobileErrorMessage" style="display:none;" class="errorMessage">Please enter the correct Domino's email.</p>
          <p class="donthaveaccount">DON'T HAVE ONE? <a class="registerlink">REGISTER NOW</a></p>
        </div>
    </div>

      
    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});
    </script>

    <script type="text/javascript">
      // global variables
      var user=null;

      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };

      //facebook api
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1487607144892582',
          xfbml      : true,
          version    : 'v2.4'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));

      function validateEmail(email) {
          var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
          return re.test(email);
      }

      function goToGame()
      {
        //alert('save the user object and go to menu page ');
        //console.log(sessionStorage.getObj('tjuser'));
        console.log('saving user info inside session storage variable as an object :)');
        sessionStorage.setObj('tjuser',user);
        window.location='menu.php';
      }

      function logoutFromTheGame()
      {
        user = '-1';
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#step2').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#step2mobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }

      function goToEmailValidation()
      {
        $('body').removeClass("loading");
        $('#step1').fadeOut('slow',function(){
          $('#step2').fadeIn('slow');
        });

        $('#step1mobile').fadeOut('slow',function(){
          $('#step2mobile').fadeIn('slow');
        });
      }

      function getMyFacebookInfo()
      {
          $('body').addClass("loading");
             FB.api('/me?fields=email,name,id,picture.type(large),first_name', function(response) {
                $('body').removeClass("loading");
                if (response && !response.error) {
                  var fbuser = response;
                  $('.playerImage').attr('src',fbuser.picture.data.url);
                  $('.playerName').html(fbuser.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
                  $('.playerName2').html(fbuser.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');
                  
                  // register user info in the db
                  $('body').addClass("loading");                  
                  $.post("engine/registeruser.php",{
                    name : fbuser.name,
                    email : fbuser.email,
                    id : fbuser.id,
                    picture : fbuser.picture.data.url,
                    phone : fbuser.phone
                  },function(data2){
                      $('body').removeClass("loading");
                      //create the user object
                      user = {
                        name : fbuser.name,
                        email : fbuser.email,
                        id : fbuser.id,
                        picture : fbuser.picture.data.url,
                        phone : fbuser.phone,
                        dominosEmail : '-1',
                        first_name : fbuser.first_name
                      };
                      // check if he is registered already
                      if(data2.status == 'alreadyRegistered' && data2.demail != '')
                      {
                          // he already entered his domino's email
                          user.dominosEmail = data2.demail;
                          goToGame();
                      } 
                      else
                      {
                          // if he is not registerd or not updated his domino's email
                          goToEmailValidation();
                      }
                  },"json").error(function(){
                      $('body').removeClass("loading");
                      noty({
                        type:'warning',
                        text:'Can not fetch your facebook account information now<br/><b>please try again later.</b>',
                        timeout: 7500,
                        layout:'topCenter',
                         animation: {
                            open: 'animated bounceInLeft',
                            close: 'animated zoomOutLeft',
                            easing: 'swing',
                            speed: 500
                        }
                      });
                      return;
                  });
                      
                }
                else
                {
                  $('body').removeClass("loading");
                  noty({
                    type:'warning',
                    text:'Could not get your facebook account information <br/> <b>please try again</b>.',
                    timeout: 7500,
                    layout:'topCenter',
                     animation: {
                        open: 'animated bounceInLeft',
                        close: 'animated zoomOutLeft',
                        easing: 'swing',
                        speed: 500
                    }
                  });
                  return;
                }
            });
      }

      $(function(){

          //goToEmailValidation();


          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });

          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });

          $('.facebookconnect').click(function(e){
            e.preventDefault();
            $('body').addClass("loading");
          
              FB.getLoginStatus(function(response) {
                  
                  $('body').removeClass("loading");

                  if (response.status === 'connected') {
                    //I am logged in
                    getMyFacebookInfo();
                  } else {
                    FB.login(function (response) {
                      if (response.authResponse) {
                        // I am logged in
                        getMyFacebookInfo();
                      } else {

                        noty({
                          type:'warning',
                          text:'Please <b>sign in</b> using your facebook account to continue!',
                          timeout: 5500,
                          layout:'topCenter',
                           animation: {
                              open: 'animated bounceInLeft',
                              close: 'animated zoomOutLeft',
                              easing: 'swing',
                              speed: 500
                          }
                        });
                        return;
                      }
                    }, {scope: 'email'});
                  }
              });
          });

          
          $('.loginbtnformobile').click(function(){
            var e = $.Event("keypress");
            e.which = 13; // # Some key code value
            $("#dominosEmailmobile").trigger(e);
          });


          $('#dominosEmail').keypress(function (e) {
            if (e.which == 13) {
              $('#dominosEmailErrorMessage').hide();
              var email = $('#dominosEmail').val();
              if(!validateEmail(email))
              {
                noty({
                  type:'warning',
                  text:'Please enter a <b>valid email</b>!',
                  timeout: 3500,
                  layout:'center',
                   animation: {
                      open: 'animated bounceInLeft',
                      close: 'animated zoomOutLeft',
                      easing: 'swing',
                      speed: 500
                  }
                });
                return;
              }
               $('body').addClass("loading");
                //check if user email registered in Domino's servers.
                $.post("engine/checkdominosemail.php",{email:email},function(data){
                  //alert(data);
                  if(data.IsRegistered == true)
                  {
                      user.dominosEmail = email;
                      //update user domino's email
                      $.post("engine/updateuserdominosemail.php",{email:user.email,dominos:email},function(data){
                        // go to the game page
                        goToGame();
                      },"json");
                  }
                  else
                  {
                      $('body').removeClass("loading");
                      $('#dominosEmailErrorMessage').show();
                      return;
                  }
                },"json");
            }
          });

          $('#dominosEmailmobile').keypress(function (e) {
            if (e.which == 13) {
              $('#dominosEmailmobileErrorMessage').hide();
              var email = $('#dominosEmailmobile').val();
              if(!validateEmail(email))
              {
                noty({
                  type:'warning',
                  text:'Please enter a <b>valid email</b>!',
                  timeout: 3500,
                  layout:'topCenter',
                   animation: {
                      open: 'animated bounceInLeft',
                      close: 'animated zoomOutLeft',
                      easing: 'swing',
                      speed: 500
                  }
                });
                return;
              }
               $('body').addClass("loading");
                //check if user email registered in Domino's servers.
                $.post("engine/checkdominosemail.php",{email:email},function(data){
                  //alert(data);
                  if(data.IsRegistered == true)
                  {
                      user.dominosEmail = email;
                      //update user domino's email
                      $.post("engine/updateuserdominosemail.php",{email:user.email,dominos:email},function(data){
                        // go to the game page
                        goToGame();
                      },"json");
                  }
                  else
                  {
                      $('body').removeClass("loading");
                      $('#dominosEmailmobileErrorMessage').show();
                      return;
                  }
                },"json");
            }
          });
        
          
          $(document).on('click','.registerlink',function(e){
            e.preventDefault();
            window.top.location.href = 'https://www.dominos.com.my/signup';
          });
        
      });
    </script>



  </body>
</html>
    
