<!doctype html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Domino's Cheesy Burst</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/landing_old.css">

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      <link href="assets/jquery.bxslider.css" rel="stylesheet" />

      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.rwdImageMaps.min.js"></script>
      <script src="assets/js/jquery.scrollTo.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>


      <meta property="og:title" content="Win 1 Year Suppply of Pizza!" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="http://bit.ly/DomiknowItAll" />
      <meta property="og:description" content="Are you a DomiKnow-It-All? Play now to beat my score!" />

      <meta name="twitter:card" value="Are you a DomiKnow-It-All? Play now to beat my score and win 1 year free pizza -> http://bit.ly/DomiKnowItAll">
     
     
      <script>
         // closing button for desktop menu
         !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});

          $(function(){
            
            
            $('.navicon-button').click(function(e){
              e.preventDefault();
              $(this).toggleClass("open");
              $('body').toggleClass('menushown');
            });

            $('.overlay_menu_div').click(function(e){
              $('.navicon-button').trigger('click');
            });

            $('#compaign').show();

            $('.desktopbackround').rwdImageMaps();
            $('.mobilebackround').rwdImageMaps();


            $("map[name=map-desktop] .order,map[name=map-mobile] .order").on('click', function (e) {
              e.preventDefault();
              e.stopPropagation();
              window.top.location.href = 'https://www.dominos.com.my/meals';
            });

            $("map[name=map-desktop] .play,map[name=map-mobile] .play").on('click', function (e) {
              e.preventDefault();
              e.stopPropagation();
              window.location = 'login.php';
            });

            $("map[name=map-desktop] .scrollDown,map[name=map-mobile] .scrollDown").on('click', function (e) {
              e.preventDefault();
              e.stopPropagation();
              $.scrollTo($(document).height()-100,1000);
            });

            $('.buttonClick3').click(function(e){
              e.preventDefault();
              e.stopPropagation();
              window.location = 'login.php';
            });

            $('.buttonClick').click(function(){
              if($(this).hasClass('promo'))
              {
                $('.buttonClick').removeClass('promo').addClass('game');
                $('.buttonClick').attr('src','assets/img/campaign/game.png');
                $('.desktopContent .left').css('left','-96.5%');
              }
              else
              {
                $('.buttonClick').removeClass('game').addClass('promo');
                $('.desktopContent .left').css('left','-5%');
                $('.buttonClick').attr('src','assets/img/campaign/promo.png');
              }
            });

            $('.buttonClick2').click(function(){
              if($(this).hasClass('promo'))
              {
                $('.buttonClick2').removeClass('promo').addClass('game');
                $('.buttonClick2').attr('src','assets/img/campaign/gamem.png');
                $('.buttonClick3').hide();
                $('.mobileContent .left').css('left','-97%');
              }
              else
              {
                $('.buttonClick2').removeClass('game').addClass('promo');
                $('.buttonClick2').attr('src','assets/img/campaign/promom.png');
                setTimeout(function(){$('.buttonClick3').show();},500);
                $('.mobileContent .left').css('left','-5%');
              }
            });

            var section = window.location.href;
            section = section.substr(section.lastIndexOf('?') + 1);
            if(section.length <= 3)
            {
              //alert('commingfromgame');
            }
            else
            {
              $('.buttonClick').trigger('click');
              $('.buttonClick2').trigger('click');
            }



              $(window).scroll(function () { 
                  var x = $('body').scrollTop();
                  $('.buttonClick').css('top',(x+100)+'px');
                  $('.buttonClick2').css('top',(x+27)+'px');
                  $('.desktopContent').css('height',$('.desktopContent .left .desktopbackround').height()+'px');
                  $('.mobileContent').css('height',$('.mobileContent .left .mobilebackround').height()+'px');
            
              });
              $("#toolbar").resize();
              

              setTimeout(function(){
                $('.desktopContent').css('height',$('.desktopContent .left .desktopbackround').height()+'px');
                $('.mobileContent').css('height',$('.mobileContent .left .mobilebackround').height()+'px');
              },6000);
            
          });

      </script>


      <!-- Dominos MY -->
      <script>
        // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        // ga('create', 'UA-64251441-1', 'auto');
        // ga('send', 'pageview');

      </script>

   </head>

  <body>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>

    <div id="compaign" style="display:none;">
      <div class="desktopContent hidden-xs">
          
          <div class="headBar">
              <div class="left2">
                <div class="navbutton">
                  <a class="navicon-button x">
                    <div class="navicon"></div>
                  </a>
                </div>
                <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
              </div>

              <div class="right">
                <div class="level" data-level="0">
                  <img class="playerImage" src="" />
                  <p class="playerName"></p>
                </div>
              </div>
          </div>

          <div class="left">
            <img class="desktopbackround" src="assets/img/campaign/desktopfull.png" usemap="#map-desktop" />
            <map name="map-desktop">
              <area shape="rect" coords="1524,38,1772,98" class="order" href="#">
              <area shape="rect" coords="1224,970,1476,1040" class="order" href="#">
              <area shape="rect" coords="324,1179,576,1244" class="play" href="#">
              <!-- <area shape="circle" coords="815,487,150" class="scrollDown" href="#"> -->
            </map>
            <img class="buttonClick game" src="assets/img/campaign/game.png">
          </div>
      </div>

      <div class="mobileContent visible-xs">
          <div class="left">
            <img class="mobilebackround" src="assets/img/campaign/mobilefull.png" usemap="#map-mobile" />
            <map name="map-mobile">
              <area shape="rect" coords="455,16,592,53" class="order" href="#">
              <area shape="rect" coords="316,798,452,834" class="order" href="#">
              <!-- <area shape="rect" coords="85,1182,220,1219" class="play" href="#"> -->
              <!-- <area shape="circle" coords="815,487,150" class="scrollDown" href="#"> -->
            </map>
            <img class="buttonClick2 game" src="assets/img/campaign/gamem.png">
            <img class="buttonClick3 stickyplay" src="assets/img/campaign/mobileplay.png">
          </div>
      </div>
    </div>

      
      
  </body>
</html>
    
