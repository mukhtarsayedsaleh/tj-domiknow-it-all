<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <title>Domino's A-Z game</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body id="body">
    

    <div class="timeline">
    <div class="timeline-bar">
      <div class="timeline-point point-1" data-tooltip="2010"></div>        
    </div>
  </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <script>


      $(document).ready(function(){

        var barWidth = $('.timeline-bar').width();
        var step = barWidth / 180;

        var tick = setInterval(function(){
          var current = $('.point-1').position().left;
          var next = current+step;

          if(next >= barWidth)
          {
            $('.point-1').css('left',barWidth+'px');
            $('.point-1').attr('data-tooltip','You are looser');
            alert('game over');
            clearInterval(tick);
            return;
          }

          $('.point-1').css('left',next+'px');
          $('.point-1').attr('data-tooltip','You can do it');

        },1000);
          
      });
    </script>

  </body>
</html>