<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <title>Domino's A-Z game</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body id="body">
    <div class="loading_div"></div>
    <a class="btn btn-primary logintofacebook">facebook login</a>
    <br/><br/>
    <a class="btn btn-primary sharefacebook">facebook share</a>
    <br/><br/>
    <a class="btn btn-primary sharetwitter">twitter share</a>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="assets/prettyLoader/js/jquery.prettyLoader.js"></script>
    <script>
      // //twitter api
      // window.twttr = (function(d, s, id) {
      //   var js, fjs = d.getElementsByTagName(s)[0],
      //     t = window.twttr || {};
      //   if (d.getElementById(id)) return t;
      //   js = d.createElement(s);
      //   js.id = id;
      //   js.src = "https://platform.twitter.com/widgets.js";
      //   fjs.parentNode.insertBefore(js, fjs);
       
      //   t._e = [];
      //   t.ready = function(f) {
      //     t._e.push(f);
      //   };
       
      //   return t;
      // }(document, "script", "twitter-wjs"));


      //facebook api
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1487607144892582',
          xfbml      : true,
          version    : 'v2.4'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));

      function gotoplay()
      {
          $('body').removeClass("loading");
          alert('game should start');
      }

      function loginToStartGame()
      { 
           $('body').addClass("loading");
           FB.api('/me?fields=email,name,id', function(response) {
              $('body').removeClass("loading");
              if (response && !response.error) {
                var user = response;
                $('body').addClass("loading");
                //check if user email registered in Domino's servers.
                $.post("engine/checkdominosemail.php",{email:user.email},function(data){
                    if(data.status == 'ok')
                    {
                        // register user info in the db
                        $.post("engine/registeruser.php",{
                          name : user.name,
                          email : user.email,
                          id : user.id,
                          phone : user.phone
                        },function(data2){
                            // then go to play
                            gotoplay();
                        },"json").error(function(){
                            $('body').removeClass("loading");
                            noty({
                              type:'warning',
                              text:'Can not play now<br/><b>please try again alter.</b>',
                              timeout: 7500,
                              layout:'topCenter',
                               animation: {
                                  open: 'animated bounceInLeft',
                                  close: 'animated zoomOutLeft',
                                  easing: 'swing',
                                  speed: 500
                              }
                            });
                            return;
                        });
                    }
                    else
                    {
                        $('body').removeClass("loading");
                        noty({
                          type:'warning',
                          text:'Please enter the <b>correct Domino\'s email</b>. Or register',
                          timeout: 7500,
                          layout:'topCenter',
                           animation: {
                              open: 'animated bounceInLeft',
                              close: 'animated zoomOutLeft',
                              easing: 'swing',
                              speed: 500
                          }
                        });
                        return;
                    }
                },"json");
              }
              else
              {
                $('body').removeClass("loading");
                noty({
                  type:'warning',
                  text:'Could not get your facebook account information <br/> <b>please try again</b>.',
                  timeout: 7500,
                  layout:'topCenter',
                   animation: {
                      open: 'animated bounceInLeft',
                      close: 'animated zoomOutLeft',
                      easing: 'swing',
                      speed: 500
                  }
                });
                return;
              }
          });
      }

      

      $(document).ready(function(){

          $('.logintofacebook').click(function(e){
              e.preventDefault();
              $('body').addClass("loading");
          
              FB.getLoginStatus(function(response) {
                  
                  $('body').removeClass("loading");

                  if (response.status === 'connected') {
                    //I am logged in
                    loginToStartGame();
                  } else {
                    FB.login(function (response) {
                      if (response.authResponse) {
                        // I am logged in
                        loginToStartGame();
                      } else {

                        noty({
                          type:'warning',
                          text:'Please <b>sign in</b> using your facebook account to continue!',
                          timeout: 5500,
                          layout:'topCenter',
                           animation: {
                              open: 'animated bounceInLeft',
                              close: 'animated zoomOutLeft',
                              easing: 'swing',
                              speed: 500
                          }
                        });
                        return;
                      }
                    }, {scope: 'email'});
                  }
              });
          });


        $('.sharefacebook').click(function(e){
              e.preventDefault();
              $('body').addClass("loading");
              FB.ui(
              {
                method: 'share',
                href: 'https://developers.facebook.com/docs/',
              },
              // callback
              function(response) {
                 $('body').removeClass("loading");
                if (response && !response.error_message) {
                  alert('Posting completed.');
                } else {
                  alert('Error while posting.');
                }
              }
            );
        });

        $('.sharetwitter').click(function(e){
              e.preventDefault();
              $('body').addClass("loading");
              window.open("https://twitter.com/intent/tweet?text="+encodeURIComponent(document.title)+"&url="+ encodeURIComponent(document.URL)+"");
        });



      });
    </script>

  </body>
</html>