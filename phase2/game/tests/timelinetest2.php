<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <title>Domino's A-Z game</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body id="body">
    

    <div class="timeline2">
    <div class="timeline2-bar">
      <div class="timeline2-point point2-1" data-tooltip="2010"></div>        
    </div>
  </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <script>


      $(document).ready(function(){

        var barHeight = $('.timeline2-bar').height();
        var step = barHeight / 10;

        var tick = setInterval(function(){
          var current = $('.point2-1').position().top;
          var next = current+step;

          if(next >= barHeight)
          {
            $('.point2-1').css('top',barHeight+'px');
            $('.point2-1').attr('data-tooltip','You are looser');
            alert('game over');
            clearInterval(tick);
            return;
          }

          $('.point2-1').css('top',next+'px');
          $('.point2-1').attr('data-tooltip','You can do it');

        },1000);
          
      });
    </script>

  </body>
</html>