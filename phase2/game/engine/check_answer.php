<?php 
    // check if the answer correct or not
    $q = $_POST['q'];
    $a = $_POST['a'];

    $questions = array(
        'a' => '4',
        'b' => '2',
        'c' => '4',
        'd' => '1',
        'e' => '3',
        'f' => '4',
        'g' => '4',
        'h' => '1',
        'i' => '3',
        'j' => '4',
        'k' => '1',
        'l' => '3',
        'm' => '2',
        'n' => 'all',
        'o' => '3',
        'p' => '3',
        'q' => '2',
        'r' => '1',
        's' => '3',
        't' => '4',
        'u' => '4',
        'v' => '1',
        'w' => '3',
        'x' => '4',
        'y' => '2',
        'z' => '3'
  );
  
  // var_dump($questions[$q]);
  // var_dump($a);
    
  if($questions[$q] == $a || $questions[$q] == 'all')
  {
    echo json_encode(array('status'=>'correct'));
  }
  else
  {
    echo json_encode(array('status'=>'wrong'));
  }


?>