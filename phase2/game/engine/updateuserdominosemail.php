<?php
include 'config.php';

if(!empty($_POST)) {
	$user_arr = array();
	$user_arr['dominos'] = $_POST['dominos'];
	$user_arr['email'] = $_POST['email'];

	// check if user already registered
	$user = ORM::for_table('user')->where(array('email'=>$user_arr['email']))->find_one();
	$user->dominos_mail = $user_arr['dominos'];	
	try
	{
		$user->save();
		echo json_encode(array('status' => 'ok'));
	}
	catch(Exception $e){
		echo $e;
	}
	
}