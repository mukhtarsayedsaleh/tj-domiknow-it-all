<?php
include 'config.php';

function check_user($uid) {
	$user = R::findOne('users', ' facebook_id = ? ', array($uid));

	if($user) {
		return true;
	}

	return false;
}

$uid = $_POST['id'];

if(check_user($uid)) {
	$user = R::findOne('users', ' facebook_id = ? ', array($uid));
	$user->played = $user->played + 1;
	R::store($user);

	echo json_encode(array('error'=>'0'));
} else {
	echo json_encode(array('error'=>'1'));
}