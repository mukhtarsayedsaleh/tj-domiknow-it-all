<?php
include 'config.php';

if(!empty($_POST)) {
	$user_arr = array();
	$user_arr['email'] = $_POST['u'];
	//get id of this user
	$user = ORM::for_table('user')->where(array('email'=>$user_arr['email']))->find_one();
	$userid = $user->id;
	//reduce lifes of this user
	$life = ORM::for_table('user_lifes')->create();
	$life->user_id = $userid;
	$life->life = -1;
	$life->source = 'game';
	$life->life_date = date('Y-m-d H:i:s', strtotime('+8 hours'));
	try
	{
		$life->save();
	}
	catch(Exception $e){
		echo $e;
	}
	
}