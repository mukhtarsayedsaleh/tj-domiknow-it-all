<?php 
	
	$email = $_POST['email'];
 
    // is cURL installed yet?
    if (!function_exists('curl_init')){
        
        die('Sorry cURL is not installed!');

    }

    // OK cool - then let's create a new cURL resource handle
    $ch = curl_init();

    // Now set some options (most are optional)
 
    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, 'https://api.dominos.com.my/api/GameContest/VerifyUserRegistration');
 
    // Set a referer
    curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");

    // User agent
    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
 
    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
 
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch,CURLOPT_POST,true);
 

	$data = array('Email' => $email,
	        'AppKey' => 'DominosFBGame'); 
	                                                                 
	$data_string = json_encode($data);    
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'Content-Type: application/json',                                                                                
	    'Content-Length: ' . strlen($data_string))                                                                       
	);

    // Download the given URL, and return output
    //$output = curl_exec($ch);

    if( ! $output = curl_exec($ch)) 
    { 
        echo curl_error($ch); 
    } 
    echo $output ;
    // Close the cURL resource, and free system resources
    curl_close($ch);

?>