<?php
include 'config.php';
session_start();

if(!empty($_POST)) {
	$user_arr = array();
	$user_arr['name'] = $_POST['name'];
	$user_arr['email'] = $_POST['email'];
	$user_arr['phone'] = $_POST['phone'];
	$user_arr['picture'] = $_POST['picture'];
	$user_arr['id'] = $_POST['id'];


	// check if user already registered
	$user = ORM::for_table('user')->where_any_is(array(array('email'=>$user_arr['email']),array('facebook_id'=>$user_arr['id'])))->find_one();

	if($user) {
		$_SESSION["loggedinuserID"] = $user['id'];
		echo json_encode(array('status'=>'alreadyRegistered','demail'=>$user['dominos_mail']));
		return;
	}


	//register the user in db.
	$new_user = ORM::for_table('user')->create();
	$new_user->name = $user_arr['name'];
	$new_user->email = $user_arr['email'];
	//$new_user->dominos_mail = $user_arr['email'];
	$new_user->phone = $user_arr['phone'];
	$new_user->picture = $user_arr['picture'];
	$new_user->facebook_id = $user_arr['id'];
	try
	{
		$new_user->save();
	}
	catch(Exception $e){
		echo $e;
	}
	
	$uid = $new_user->id();
	$_SESSION["loggedinuserID"] = $uid;

	//give the new user 3 lifes
	$new_user_life = ORM::for_table('user_lifes')->create();
	$new_user_life->user_id = $uid;
	$new_user_life->life = '3';
	$new_user_life->source = 'register';
	$new_user_life->life_date = date("Y:m:d H:i:s", strtotime('+8 hours'));

	//var_dump($uid);
	try
	{
		$new_user_life->save();
	}
	catch(Exception $e){
		echo $e;
	}

	echo json_encode(array('status' => 'ok'));
}