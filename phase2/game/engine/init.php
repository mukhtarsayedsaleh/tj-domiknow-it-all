<?php
include 'config.php';

if(!empty($_POST)) {
	$user_arr = array();
	$user_arr['email'] = $_POST['u'];
	//get id of this user
	$user = ORM::for_table('user')->where(array('email'=>$user_arr['email']))->find_one();
	$userid = $user->id;


	//get current week begin and end
	$people = ORM::for_table(null)->raw_query('SELECT `weekname` , date(start) as `start`, date(end) as `end` FROM week where now() >= start and now() <= end')->find_one();
	$thisweekstart = $people['start'];
	$thisweekend = $people['end'];

	//get lifes of this user during this week of campaign
	$lifes =ORM::for_table(null)
                ->raw_query('select sum(T.life) as `sum` from (SELECT `life`,`source`,Date(`life_date`)
								FROM user_lifes where 
								user_id = '.$userid.' 
								and `source` = \'game\'
								and (`life_date` between \''.$thisweekstart.' 00:00:00\' and \''.$thisweekend.' 23:59:59\')

								union all


								SELECT `life`,`source`,Date(`life_date`)
								FROM user_lifes where
								user_id = '.$userid.' 
								and `source` <> \'game\' 
								and (`life_date` between \''.$thisweekstart.' 00:00:00\' and \''.$thisweekend.' 23:59:59\') group by source,DATE(`life_date`)) T')
                ->find_one()
                ['sum'];
	echo json_encode(array('lifes'=>$lifes));
	
}