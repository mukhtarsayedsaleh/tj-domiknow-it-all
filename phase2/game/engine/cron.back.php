<?php 
		include 'config.php';

		//Domino's A-Z game cron job algorithm

		//1- get start date and end date for this week and last week
			 //get current week begin and end
		      $week = ORM::for_table(null)->raw_query('SELECT `id`, `weekname` , date(start) as `start`, date(end) as `end` FROM week where now() >= start and now() <= end')->find_one();
		      $thisweekid = $week['id'];
		      $thisweekstart = $week['start'];
		      $thisweekend = $week['end'];
		      echo 'This Week Boundries: <br/> id:'.$thisweekid.'<br/>start:'.$thisweekstart.'<br/> end:'.$thisweekend;

		      //get last week begin and end
		      $lastweekid=$thisweekid-1;
		      $l_week = ORM::for_table(null)->raw_query('SELECT `id`, `weekname` , date(start) as `start`, date(end) as `end` FROM week where `id` = '.$lastweekid)->find_one();
		      $lastweekid = $l_week['id'];
		      $lastweekstart = $l_week['start'];
		      $lastweekend = $l_week['end'];
		      echo '<br/>Last Week Boundries: <br/> id:'.$lastweekid.'<br/>start:'.$lastweekstart.'<br/> end:'.$lastweekend;

		//2- for each user     
			$users = ORM::for_table('user')->find_many();
			$_users = array();
			foreach($users as $u)
			{	
				//get basic info for this user to use it later
				echo '<h4>Getting info for each user</h4>';
				$_users[$u['id']]['userid']=$u['id'];
				$_users[$u['id']]['dominos_email']=$u['dominos_mail'];
				$_users[$u['id']]['picture']=$u['picture'];
				$_users[$u['id']]['name']=$u['name'];
				$_users[$u['id']]['score_of_game']=0;
				$_users[$u['id']]['score_of_share']=0;

				//how many pizzas have he bought last week?
				$multiplier = 1;
			    $ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL, 'https://api.dominos.com.my/api/GameContest/GetCustomerCheeseBurstInfo');
			    curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
			    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
			    curl_setopt($ch, CURLOPT_HEADER, 0);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			    curl_setopt($ch,CURLOPT_POST,true);
			    $data = array('Email' => $u['dominos_mail'], 
			                    'AppKey' => 'DominosFBGame',
			                    'StartDate' => $lastweekstart.'', 
			                    'EndDate' => $lastweekend.'');                                               
			    $data_string = json_encode($data);    
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			        'Content-Type: application/json',                                                                                
			        'Content-Length: ' . strlen($data_string))                                                                       
			    );
			    if( ! $output = curl_exec($ch)) 
			    { 
			        echo curl_error($ch); 
			    }
			    $largePizzas = json_decode($output)->CheeseBurstLargeTotalCount;
			    $regularPizzas = json_decode($output)->CheeseBurstRegularTotalCount;
			    $pizzas = (2 * $regularPizzas) + (3 * $largePizzas);
			    if($pizzas >= 2)
			    {
			      $multiplier=$pizzas;
			    }
			    curl_close($ch);

			    //set the multiplier for last week for this user depending of the number of pizzas that he has bought
			    $_users[$u['id']]['multiplier'] = $multiplier;


			    //get the score of last week sharing for this user
				$sql2 = "select ifnull(sum(T.score),0) as `score` from (SELECT `score`,`score_source`,Date(`score_date`)
                                     FROM user_score where
                                     user_id = ".$u['id']." 
                                     and (`score_source` = 'Share twitter' or `score_source` = 'Share facebook' )
                                     and (`score_date` between '".$lastweekstart." 00:00:00' and '".$lastweekend." 23:59:59') group by score_source,DATE(`score_date`)) T";
				$sharescore = ORM::for_table(null)->raw_query($sql2)->find_one();
				$_users[$u['id']]['score_of_share'] = $sharescore['score'];

				//get the high score of last week games for this user
				$sql = "SELECT max(`score`) as `score` FROM user_score where `score_source`='game' and (`score_date` between '".$lastweekstart." 00:00:00' and '".$lastweekend." 23:23:59') and `user_id` = ".$u['id'];
				$gamescore = ORM::for_table(null)->raw_query($sql)->find_one();
				$_users[$u['id']]['score_of_game'] = $gamescore['score'];




				//Adding 3 lifes of this week for this user 
				echo '<h4>Adding 3 lifes of this week for this user ('.$u['email'].')</h4>';
				
				$life = ORM::for_table('user_lifes')->create();
				$life->user_id = $u['id'];
				$life->life = 3;
				$life->source = 'new week';
				$life->life_date = date('Y-m-d H:i:s', strtotime('+8 hours'));
				try
				{
					$life->save();
				}
				catch(Exception $e){
					echo $e;
				}

				echo '<hr/>';
			}



		//3- make the winner list and leader board of last week
			
			$users = $_users;
			foreach($users as $u)
			{
				//calculate the final scores and store it with user info in temporary table
				$final = ( $u['score_of_game'] + $u['score_of_share'] ) * $u['multiplier'];

				$temp_result = ORM::for_table('temp_results')->create();
				$temp_result->user_id = $u['userid'];
				$temp_result->name = $u['name'];
				$temp_result->score = $final;
				$temp_result->picture = $u['picture'];
				try
				{
					$temp_result->save();
				}
				catch(Exception $e){
					echo $e;
				}

			}
			//get the top 3 of the temporary table and store it in the actual table as 'last week winners'
			$sql = "select * from temp_results order by score desc limit 0,3";
			$winners = ORM::for_table(null)->raw_query($sql)->find_many();
			$i=0;
			foreach($winners as $w)
			{
				$winner = ORM::for_table('winners')->create();
				$winner->user_id = $w['user_id'];
				$winner->name = $w['name'];
				$winner->score = $w['score'];
				$winner->picture = $w['picture'];
				$winner->weekid = $lastweekid;
				try
				{
					$winner->save();
				}
				catch(Exception $e){
					echo $e;
				}
			}


			//get the top 10 of the temporary table and store it in the actual table as 'last week leaderboard'
			//commented after dalers suggestion
				// $sql = "select * from temp_results order by score desc limit 0,10";
				// $leaders = ORM::for_table(null)->raw_query($sql)->find_many();
				// $i=0;
				// foreach($leaders as $l)
				// {
				// 	$leader = ORM::for_table('leaders')->create();
				// 	$leader->user_id = $l['user_id'];
				// 	$leader->name = $l['name'];
				// 	$leader->score = $l['score'];
				// 	$leader->picture = $l['picture'];
				// 	$leader->weekid = $lastweekid;
				// 	try
				// 	{
				// 		$leader->save();
				// 	}
				// 	catch(Exception $e){
				// 		echo $e;
				// 	}
				// }

			//truncate the temporary table :)
			$temp = ORM::for_table('temp_results')->delete_many();

	
?>