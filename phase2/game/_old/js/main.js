$(function () {
	$('.fancybox').fancybox({wrapCSS:'orange'});

	window._sarpercent = 0;
	window.clicked = 0;
	window.opened = 0;

	$('.item').click(function (e) {
		if(window.loose == 1) {
			alert('You lost, please start game again');
			return;
		}

		var box = e.currentTarget;
		var id = $(box).data('related');

		window.clicked += 1;
		if (window.clicked == 3) {
			window.clicked = 1;
			closeAll();
			$(box).attr('data-gaming', '1');
		} else if (window.clicked == 2) {
			if ($(box).attr('data-gaming') != '1') {
				if (parseInt(window.lastId) == parseInt(id)) {
					$('.c' + id).attr('data-opened', 1);
					window.opened += 1;
				}
			}
		}

		$(box).css('background-image', 'url(img/puzzle/' + id + '.png)');
		$(box).attr('data-gaming', '1');
		window.lastId = id;

		if (window.opened == 15) {
			window.loose = 1;
			var score = $(".game-timer").text();

			$('#game_points').text(parseInt(score)*5);

			if(score > 120) {
				alert('Cheating is not good');
				return;
			}

			$.ajax({
				url : window._ajaxUrl + 'get_user_submission.php?'+ "preventCache="+new Date(),
				data : {id:window._userID, score:score},
				cache : false,
				type: 'post',
				dataType : 'json',
				beforeSend: function(){
					$.fancybox.open('img/loader.gif', {
						wrapCSS:'reset',
						modal:true
					});
				},
				success: function(data) {
					$.fancybox.close();

					if(data.message) {
						alert(data.message);
						return;
					}

					if(data.count == null) data.count = 0;

					$('#submission_points').text(data.count);
					if(data.friends == 1) {
						$('#friends_points').text(10);
					}

					var friend_point = $('#friends_points').text();
					if(data.count == 0 || data.count == null) data.count = 1;

					$('#total_number_win').text(((parseInt(score)*5)+parseInt(friend_point))*data.count);
					$('#gift_description').text(data.description);

					$.fancybox.open($('#congrat'),{wrapCSS:'orange register'});
				}
			});
		}
	});

	function closeAll() {
		$('.item').each(function (i) {
			var box = this;

			if ($(box).data('opened') != 1) {
				$(box).css('background-image', 'url(img/puzzle/box.png)');
				$(box).attr('data-gaming', '0');
			}
		});
	}

	function timerJob() {
		if (window.loose == 1) return;

		var realTime = parseInt($('.game-timer').text());
		var first = 1.25;
		var second = 0.625;

		if (realTime != 0) {
			realTime -= 1;

			if (window._sarpercent < 50) {
				window._sarpercent += first;
			} else if (window._sarpercent >= 50 && window._sarpercent < 100) {
				$('#free_uno').addClass('gone');
				$('#free_second').removeClass('gone');

				window._sarpercent += second;
			}
		} else {
			window.loose = 1;
			$.fancybox.open($('#gameOver'),{modal:true,wrapCSS:'reset'});
		}

		$('.game-timer').text('' + realTime); // Convert to string
		$('#flag_moving').css('width', window._sarpercent + '%');
	}

	setInterval(timerJob, 1000);
});