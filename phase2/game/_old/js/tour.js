$(function(){
	window.loose = 1;
	$('.skip_tutorial, #finish_tutorial').on('click', function(){
		$.fancybox.close();
		window.loose = 0;
	});

	$('#next_step2').on('click', function(){
		$.fancybox.close();
		$.fancybox.open($('#step2'),{modal:true,wrapCSS:'reset'});
	});

	$('#next_step3').on('click', function(){
		$.fancybox.close();
		$.fancybox.open($('#ready'),{modal:true,wrapCSS:'reset'});
	});

	$.fancybox.open($('#step1'),{modal:true,wrapCSS:'reset'});
});