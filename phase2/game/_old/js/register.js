$(function () {
	window._ajaxUrl = 'https://twitterjaya.tv/dominos-game/game/engine/';
	function check_user(uid) {
		$.ajax({
			url: window._ajaxUrl + 'check_user.php?'+ "preventCache="+new Date(),
			type: 'post',
			data: {id: uid},
			dataType: 'json',
			success: function (data) {
				if (data.error == '1') {
					window.loose = true;
					$.fancybox.open($("#register"), { modal: true, wrapCSS: 'orange register' });
				}
			}
		});
	}

	$.getScript('//connect.facebook.net/en_US/all.js', function () {
		FB.init({
			appId: '698474370173300',
			channelUrl: 'https://twitterjaya.tv/dominos-game/game/channel.html'
		});


		window.setTimeout(function () {
			FB.Canvas.setAutoGrow();
		}, 250);

		FB.getLoginStatus(function (response) {
			if (response.status === 'connected') {
				window._userID = response.authResponse.userID;
				check_user(response.authResponse.userID);
			} else {
				FB.login(function (response) {
					if (response.authResponse) {
						window._userID = response.authResponse.userID;
						check_user(response.authResponse.userID);
						location.reload();
					} else {
						location.reload();
					}
				});
			}
		});

		$('#register_button').on('click', function (e) {
			var name = $('#register_name').val();
			var email = $('#register_email').val();
			var phone = $('#register_phone').val();

			if(name == '' || email == '' || phone == '') {
				$('#register_error').show();
				$('#register_error').html('Please fill all fields');

				return;
			}

			$.ajax({
				url: window._ajaxUrl + 'register.php'+"?preventCache="+new Date(),
				data: {id: window._userID, name: name, email: email, phone: phone},
				type: 'post',
				dataType: 'json',
				success: function (data) {
					if (data.registered == '1') {
						$.fancybox.close();
						location.reload();
					} else {
						$('#register_error').show();
						$('#register_error').html(data.message);
					}
				}
			});
		});


		$('#okay_button_friend').on('click', function (e) {
			$.fancybox.close();
			$.fancybox.open($('#congrat'),{wrapCSS:'orange register'});
		});

		$('#invite_10_friends').on('click', function (e) {
			FB.ui({method: 'apprequests',
				message: 'My Great Request',
				new_style_message: true,
				filters : ["app_non_users"],
				max_recipients : 10,
				frictionlessRequests : true
			}, function(response) {
				if(response.to.length < 9) {
					$.fancybox.open($('#friend10'), {modal:true,wrapCSS:'reset'});
				} else {
					$.ajax({
						url: window._ajaxUrl + 'friend_invite.php'+"?preventCache="+new Date(),
						data: {uid: window._userID},
						type: 'post',
						dataType: 'json',
						success: function (data) {
							if (data.error == '0') {
								$('#friends_points').text('10');
								var friend_point = $('#friends_points').text();
								var points = $('#submission_points').text();
								var score = $('#game_points').text();
								points = parseInt(points);
								friend_point = parseInt(friend_point);
								score = parseInt(score);

								$('#total_number_win').text(((parseInt(score)*5)+parseInt(friend_point))*points);
							} else {
								alert('You have already invited friends');
							}
						}
					});
				}
			});
		});
	});
});