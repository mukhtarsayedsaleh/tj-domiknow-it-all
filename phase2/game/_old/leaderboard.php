<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7"> <![endif]-->
<!--[if IE 8]>     <html class="ie8"> <![endif]-->
<!--[if IE 9]>     <html class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html> <!--<![endif]-->
<head>
<title>DOMINO'S Cheesy Crust</title>
	<meta charset="utf-8">	
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="//tjclients.my/libraries/fonts/KBReindeerGamesMedium/font.css">
	<link rel="stylesheet" href="//tjclients.my/libraries/fonts/HelveticaNeueLight/font.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<!--[if IE]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="//tjclients.my/libraries/fancybox/source/jquery.fancybox.css">
	<script src="//tjclients.my/libraries/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="js/main.js"></script>
</head>

<body>

<img src="img/leaderboard.jpg" width="810" usemap="#image-maps" alt="" />
<map name="image-maps">
	<area shape="rect" coords="247,35,296,61" href="./"/>
	<area shape="rect" coords="322,36,391,61" href="how.php"/>
	<area shape="rect" coords="640,30,790,100" href="https://www.facebook.com/DominosSG/app_1395066170733871" target="_blank">

	<area shape="rect" coords="419,37,544,63" href="leaderboard.php"/>
	<area shape="rect" coords="518,315,697,363" href="game.php"/>
	<area shape="rect" coords="562,37,603,61" href="#tnc" class="fancybox"/>
	<area shape="rect" coords="311,1019,595,1044" target="_blankf" href="http://www.dominos.com.sg"/>
	<area shape="rect" coords="618,1021,739,1055" target="_blank" href="https://itunes.apple.com/sg/app/dominos-sg/id499813546"/>
</map>
<div class="leaderboard">
	<div class="week">
		<ul id="menu" class="menu">
			<?php
			include 'engine/config.php';

			$weeks = R::findAll('weeks');
			$today = date('Y-m-d');
			$weekID = R::getAll("SELECT * FROM cheesycrust_weeks w WHERE '{$today}' between w.start and w.end");
			$weekID = $weekID[0]['id'];
			?>

			<?php foreach($weeks as $week) :?>
					<li <?php echo $weekID==$week->id?' class="active" ':'';?>><a href="#week<?php echo $week->id;?>"><?php echo $week->week;?></a></li>
			<?php endforeach;?>
		</ul>

		<?php foreach($weeks as $week) :?>
			<div id="week<?php echo $week->id;?>" class="content">
				<p style="margin: -17px 0 8px 0;color: #fff;font: normal 18px KBReindeerGamesMedium;">Week <?php echo $week->id;?> : <?php echo date('j M', strtotime($week->start));?> - <?php echo date('j M Y', strtotime($week->end));?></p>
				<div class="topten">
					<?php
						$sql = "SELECT (g.score*(IF(g.packages = 0, 1, g.packages))+(u.invited * 30)) as score, u.name as username, u.facebook_id as facebookid FROM `cheesycrust_games` g
								JOIN cheesycrust_users u on u.facebook_id = g.user_id
								WHERE g.week_id = {$week->id}
								ORDER BY score DESC
								LIMIT 0,10";

						$users = R::getAll($sql);
					?>

					<?php if(empty($users) || $users == null) :?>
						<p style="margin: 189px 0 21px 267px;color: #fff;font: normal 18px KBReindeerGamesMedium;position: absolute;width: 161px;">No Winners Yet!</p>
					<?php else:?>
						<?php
							$i = 1;
						foreach($users as $user):?>
							<div>
								<span><?php echo $i; $i++; ?></span>
								<img src="https://graph.facebook.com/<?php echo $user['facebookid'];?>/picture/?width=131&height=131">
								<p><?php echo $user['username'];?></p>
							</div>
						<?php endforeach;?>
					<?php endif;?>

				</div>
			</div>
		<?php endforeach;?>

	</div>
</div>

<script src="js/jquery.tabify.js"></script>
<script>
	$(document).ready(function () {
		$('#menu').tabify();
	});
</script>


<?php include ('popup.html'); ?>
<div id="fb-root"></div>
<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript"> 
  window.fbAsyncInit = function() {
    FB.init({appId: '300517713409327', status: true, cookie: true, xfbml: true});
	window.setTimeout(function() {
     FB.Canvas.setAutoGrow();
  }, 250);
  };
  (function() {
    var e = document.createElement('script'); e.async = true;
    e.src = document.location.protocol +
      '//connect.facebook.net/en_US/all.js';
    document.getElementById('fb-root').appendChild(e);
  }());
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36875082-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
	$.getScript('//connect.facebook.net/en_US/all.js', function () {
		FB.init({
			appId: '698474370173300',
			channelUrl: 'https://twitterjaya.tv/dominos-game/game/channel.html'
		});


		window.setTimeout(function () {
			FB.Canvas.setAutoGrow();
		}, 250);

		$('.fancybox').fancybox({wrapCSS:'orange'});


	});
</script>


<style>
	.menu { padding: 0; clear: both; }
	.menu li { display: inline; }
	.menu li a { padding: 7px; float:left;  border-bottom: none; text-decoration: none; font:normal 21px KBReindeerGamesMedium,Arial; color: #fff; font-weight: bold;}
	.menu li.active a { opacity: 1; }
	.content { float: left; clear: both;  border-top: none; border-left: none;  padding: 10px 0 20px; width: 756px; margin-top:27px; }

</style>

</body>
</html>