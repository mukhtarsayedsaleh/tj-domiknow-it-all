<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie7"> <![endif]-->
<!--[if IE 8]>
<html class="ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html> <!--<![endif]-->
<head>
    <title>DOMINO'S Cheesy Crust</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="//tjclients.my/libraries/fonts/KBReindeerGamesMedium/font.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!--[if IE]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="//tjclients.my/libraries/fancybox/source/jquery.fancybox.css">
    <script src="//tjclients.my/libraries/fancybox/source/jquery.fancybox.pack.js"></script>
</head>

<body>

<img src="img/home.jpg" width="810" usemap="#image-maps" alt=""/>
<map name="image-maps">
    <area shape="rect" coords="247,35,296,61" href="./"/>
    <area shape="rect" coords="322,36,391,61" href="how.php"/>
    <area shape="rect" coords="419,37,544,63" href="leaderboard.php"/>
	<area shape="rect" coords="640,30,790,100" href="https://www.facebook.com/DominosSG/app_1395066170733871" target="_blank">
	<area shape="rect" coords="527,355,709,400" href="game.php"/>
    <area shape="rect" coords="562,37,603,61" href="#tnc" class="fancybox"/>
    <area shape="rect" coords="311,1019,595,1044" target="_blank" href="http://www.dominos.com.sg"/>
    <area shape="rect" coords="618,1021,739,1055" target="_blank"
          href="https://itunes.apple.com/sg/app/dominos-sg/id499813546"/>
</map>
<?php include('popup.html'); ?>


<div style="display: none">
	<img src="img/puzzle/1.png">
	<img src="img/puzzle/2.png">
	<img src="img/puzzle/3.png">
	<img src="img/puzzle/4.png">
	<img src="img/puzzle/5.png">
	<img src="img/puzzle/6.png">
	<img src="img/puzzle/7.png">
	<img src="img/puzzle/8.png">
	<img src="img/puzzle/9.png">
	<img src="img/puzzle/10.png">
	<img src="img/puzzle/11.png">
	<img src="img/puzzle/12.png">
	<img src="img/puzzle/13.png">
	<img src="img/puzzle/14.png">
	<img src="img/puzzle/15.png">
</div>


<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36875082-1']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>

<iframe id='manifest_iframe_hack'
        style='display: none;'
        src='temporary_manifest_hack.html'>
</iframe>


<script>
	$.getScript('//connect.facebook.net/en_US/all.js', function () {
		FB.init({
			appId: '698474370173300',
			channelUrl: 'https://twitterjaya.tv/dominos-game/game/channel.html'
		});


		window.setTimeout(function () {
			FB.Canvas.setAutoGrow();
		}, 250);

		$('.fancybox').fancybox({wrapCSS:'orange'});


	});
</script>
</body>
</html>