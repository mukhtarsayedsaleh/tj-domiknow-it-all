<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie7"> <![endif]-->
<!--[if IE 8]>
<html class="ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html> <!--<![endif]-->
<head >
    <title>DOMINO'S Cheesy Crust</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="//tjclients.my/libraries/fonts/KBReindeerGamesMedium/font.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!--[if IE]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="//tjclients.my/libraries/fancybox/source/jquery.fancybox.css">
    <script src="//tjclients.my/libraries/fancybox/source/jquery.fancybox.pack.js"></script>

    <script src="js/register.js"></script>
    <script src="js/main.js?<?php echo rand()?>"></script>
    <script src="js/tour.js"></script>
</head>

<body>

<img src="img/game.jpg" width="810" usemap="#image-maps" alt=""/>
<map name="image-maps">
    <area shape="rect" coords="247,35,296,61" href="./"/>
    <area shape="rect" coords="322,36,391,61" href="how.php"/>
    <area shape="rect" coords="419,37,544,63" href="leaderboard.php"/>
    <area shape="rect" coords="601,139,756,176" href="game.php"/>
    <area shape="rect" coords="562,37,603,61" href="#tnc" class="fancybox"/>
	<area shape="rect" coords="640,30,790,100" href="https://www.facebook.com/DominosSG/app_1395066170733871" target="_blank">
	<area shape="rect" coords="311,1019,595,1044" target="_blank" href="http://www.dominos.com.sg"/>
    <area shape="rect" coords="618,1021,739,1055" target="_blank"
          href="https://itunes.apple.com/sg/app/dominos-sg/id499813546"/>
</map>
<div class="countdown">
    <span class="game-timer">120</span>seconds
</div>
<div class="puzzle">
    <?php $no = range(1, 15);
    $no = array_merge($no, $no);
    shuffle($no);
    foreach ($no as $n) { ?>
        <div class="item c<?php echo $n;?>" style="background-image: url(img/puzzle/box.png)" data-related="<?php echo $n;?>"></div>
    <?php } ?>
</div>
<div class="indicator">
    <span id = "flag_moving" style="width: 0%;"><img src="img/arrow.png"></span>

    <div class="time">
        <div class="left half"><span>40 SECS</span></div>
        <div class="left half"><span>120 SECS</span></div>
    </div>
</div>
<div class="freebies">
    <img id="free_uno" src="img/freebies_1.jpg" >
    <img id="free_second" src="img/freebies_2.jpg" class="gone">
</div>

<?php include('popup.html'); ?>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36875082-1']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
<div style="display: none">
	<img src="img/puzzle/1.png" >
	<img src="img/puzzle/2.png">
	<img src="img/puzzle/3.png">
	<img src="img/puzzle/4.png">
	<img src="img/puzzle/5.png">
	<img src="img/puzzle/6.png">
	<img src="img/puzzle/7.png">
	<img src="img/puzzle/8.png">
	<img src="img/puzzle/9.png">
	<img src="img/puzzle/10.png">
	<img src="img/puzzle/11.png">
	<img src="img/puzzle/12.png">
	<img src="img/puzzle/13.png">
	<img src="img/puzzle/14.png">
	<img src="img/puzzle/15.png">
</div>


</body>
</html>