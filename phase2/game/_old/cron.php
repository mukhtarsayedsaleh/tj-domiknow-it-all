<?php
$file_path = realpath(dirname(__FILE__));

if(file_exists($file_path . '/working')) die;
else file_put_contents($file_path . '/working', '1');

include $file_path . '/engine/config.php';

function get_user_info($email, $start, $end) {
	$info = file_get_contents('http://www.dominos.com.sg/gamecontest.aspx?cmd=pizza_count&email='.$email.'&fromdate='.$start.'&todate='.$end);
	$info = json_decode($info, true);

	$count = $info['countR'] * 2;
	$count += $info['countL'] * 3;

	return array(
		'count' => $count,
		'regular' => $info['countR'],
		'large' => $info['countL']
	);
}

$today = date('Y-m-d');
$users = R::findAll('users');
if ($users){
	$week = R::getAll("SELECT * FROM weeks w WHERE '{$today}' between w.start and w.end");
	$day_start = date('Y/m/d', strtotime($week[0]['start']));
	$day_end = date('Y/m/d', strtotime($week[0]['end']));
	$week = $week[0]['id'];

	foreach($users as $user) {
        echo $user->facebook_id . ' ' . $user->email . "\n";
		$game = R::findOne('games', ' week_id = ? AND user_id = ? ', array($week, $user->facebook_id));

		if($game) {
            $count = get_user_info($user->email, $day_start, $day_end);

			$game->packages = $count['count'];
			$game->regular = $count['regular'];
			$game->large = $count['large'];
			R::store($game);
		}
	}
}

unlink($file_path . '/working');