<?php
include 'config.php';

function save_user_invite($uid) {
	$user = R::findOne('users', ' facebook_id = ? ', array($uid));
	$result = array();

	if($user->invited != 1) {
		$user->invited = 1;
		R::store($user);
		$result = array('error'=>'0');

		return json_encode($result);

	}
	$result = array('error'=>'1');

	return json_encode($result);
}

$uid = $_POST['uid'];

echo save_user_invite($uid);