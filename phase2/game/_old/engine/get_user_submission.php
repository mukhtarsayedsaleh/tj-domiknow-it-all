<?php
include 'config.php';

function get_user_submission($uid, $score) {
	$gift_description = array(
		'CB01' => 'Free Breadstix',
		'CB02' => 'Free Twisty Bread',
		'CB03' => 'Free Cinnastix',
		'CB04' => 'Free Garlic Cheese Onion Rings',
		'CB05' => '50% Golden Roasted Drummets',
		'CB06' => '50% Off Crazy Chicken Crunchies'
	);

	$today = date('Y-m-d');

	$user = R::findOne('users', ' facebook_id = ? ', array($uid));

	if($score >= 120) return json_encode(array('message'=>'Please do not cheat'));

	$sql = "SELECT *, w.id as weekid FROM games as g
			JOIN weeks as w ON g.week_id = w.id
			WHERE g.user_id = {$uid} AND (w.start <= '{$today}' AND '{$today}' <= w.end)";

	$rows = R::getRow($sql);

	if(empty($rows)) {
		$week = R::getRow("SELECT * FROM weeks w WHERE ('{$today}' between w.start and w.end)");
		$game = R::dispense('games');
		$game->week_id = $week['id'];
	} else {
		$game = R::findOne('games', ' week_id = ? AND user_id = ? ', array($rows['weekid'], $uid));
	}

	$game->user_id = $uid;

	if($game) {
		if($game->score <= ($score*5)) {
			$game->score = $score*5;

			R::store($game);
		}
	}

	$count = $game->packages;
	$friends = $user->invited;

	$gift = give_present($score, $user->email);

	$gifts = R::dispense('won');
	$gifts->user_id = $uid;
	$gifts->gift = $gift_description[$gift];
	R::store($gifts);

	return json_encode(array('count'=>$count, 'friends'=>$friends, 'gift'=>$gift, 'description'=>$gift_description[$gift]));
}

function give_present($score, $mail) {
	$what_to_win = array();

	if($score < 120 && $score >= 80) {
		$what_to_win[] = 'CB05';
		$what_to_win[] = 'CB06';
	} elseif ($score < 80 && $score > 0) {
		$what_to_win[] = 'CB01';
		$what_to_win[] = 'CB02';
		$what_to_win[] = 'CB03';
		$what_to_win[] = 'CB04';
	}

	if($score != 0) {
		$gift = array_rand($what_to_win);
		$gift = $what_to_win[$gift];
		file_get_contents('http://www.dominos.com.sg/gamecontest.aspx?cmd=credit_coupon&email='.$mail.'&coupon='.$gift);

		return $gift;
	}

	return false;
}


$uid = $_POST['id'];
$score = $_POST['score'];

echo get_user_submission($uid, $score);