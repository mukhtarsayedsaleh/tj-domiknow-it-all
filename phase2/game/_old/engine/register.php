<?php
include 'config.php';

function register_user($user) {
	$new_user = R::dispense('users');

	$new_user->name = $user['name'];
	$new_user->email = $user['email'];
	$new_user->phone = $user['phone'];
	$new_user->facebook_id = $user['id'];

	R::store($new_user);

	return true;
}

function check_email($email) {
	$result = file_get_contents('http://www.dominos.com.sg/gamecontest.aspx?cmd=validate_email&email='.$email);
	$result = json_decode($result, true);

	if(isset($result['success'])) return true;

	return false;
}

// Creating new user
if(!empty($_POST)) {
	$user_arr = array();
	$user_arr['name'] = $_POST['name'];
	$user_arr['email'] = $_POST['email'];
	$user_arr['phone'] = $_POST['phone'];
	$user_arr['id'] = $_POST['id'];

	$user = R::findOne('users', ' facebook_id = ? OR email = ? ', array($user_arr['id'], $user_arr['email']));

	if($user) {
		$error = array(
			'message' => 'This email is already registered'
		);

		echo json_encode(array_merge(array('registered' => '0'), $error));
		die;
	}

	if(check_email($_POST['email'])) {
		register_user($user_arr);
		echo json_encode(array('registered' => '1'));
		die;
	} else {
		$error = array(
			'message' => 'Your email is not registered with Domino\'s Pizza Singapore. Please head over to website for registration'
		);

		echo json_encode(array_merge(array('registered' => '0'), $error));
	}
}