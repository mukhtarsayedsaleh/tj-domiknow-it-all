<!doctype html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Domino's A-Z Game</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      


      <script src="assets/js/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     


     

   </head>

  <body>
    
    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>

    <div class="desktopContent hidden-xs">
        <div id="step2">
          <div class="headBar">
              <div class="left">
                <div class="navbutton">
                  <a class="navicon-button x">
                    <div class="navicon"></div>
                  </a>
                </div>
                <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
              </div>

              <div class="right">
                <div class="level" data-level="0">
                  <img class="playerImage" src="" />
                  <p class="playerName"></p>
                </div>
              </div>
          </div>
          <img class="logo" src="assets/img/login/logo_desktop.png" />
          <img class="step2instruction" src="assets/img/menu/instruction-desktop.png" />
          <img class="facebookconnect hvr-float" src="assets/img/menu/playnow.png" />
         
        </div>
    </div>
































    <div class="mobileContent visible-xs">
        <div id="step2mobile">
          <div class="headBar">
              <div class="right">
                <div class="level" data-level="0">
                  <img class="playerImage" src="" />
                </div>
                <br/><br/><br/>
                <p class="playerName2"></p>
              </div>
          </div>
          <img class="logo img-responsive" src="assets/img/menu/logo-mobile.png" />
          <img class="btn-menu-mobile img-responsive facebookconnect" src="assets/img/menu/playnowmobile.png" />
          <img class="btn-menu-mobile img-responsive howtopage" src="assets/img/menu/howtomobile.png" />
          <img class="btn-menu-mobile img-responsive pointsystem" src="assets/img/menu/pointsystemmobile.png" />
          <img class="btn-menu-mobile img-responsive prizespage" src="assets/img/menu/prizesmobile.png" />
          <img class="btn-menu-mobile img-responsive topsecret" src="assets/img/menu/topsec.png" />
          <!-- <img class="btn-menu-mobile img-responsive winnersmobile" src="assets/img/menu/winnerlistmobile.png" /> -->
          <img class="btn-menu-mobile img-responsive leadersmobile" src="assets/img/menu/leaderboardmobile.png" />
          <img class="btn-menu-mobile img-responsive tacpage" src="assets/img/menu/tcmobile.png" />
          <br/>
          <br/>
          <br/>
          <br/>
      
        </div>
        

    </div>

    <!-- <img class="circlelogoutbtn btn-menu-mobile visible-xs" src="assets/img/logoutcircle.png" /> -->

      
    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});
    </script>

    <script type="text/javascript">

      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };

      // global variables
      var user=sessionStorage.getObj('tjuser');
      console.log(user);
      //prevent cheating
      if(user == null)
      {
        window.location = 'login.php';
      }
     
      // load user info
      try
      {
        $('.playerImage').attr('src',user.picture);
        $('.playerName').html(user.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
        $('.playerName2').html(user.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');
      }catch(e){}

      function logoutFromTheGame()
      {
        user = '-1';
        try
        {
          //FB.logout();
        }catch(e){}  
        sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#step2').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#step2mobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }

      
      
      $(function(){
          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });

          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          $('.howtopage').click(function(e){
            window.location='howto.php';
          });

          $('.prizespage').click(function(e){
            window.location='prizes.php';
          });

          $('.topsecret').click(function(e){
            window.location='topsecret.php';
          });

          $('.tacpage').click(function(e){
            window.location='tac.php';
          });

          $('.pointsystem').click(function(e){
            window.location='pointsystem.php';
          });

          $('.winnersmobile').click(function(e){
            window.location='winners.php';
          });

          $('.leadersmobile').click(function(e){
            window.location='leaderboard.php';
          });

          $('.facebookconnect').click(function(e){
            e.preventDefault();
            $('body').addClass("loading");
            window.location='game.php';
          });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });
      });
    </script>

   

  </body>
</html>
    
