<?php 
  set_time_limit (0);
  include('engine/config.php');
  //sorting function based on final scores
  session_start();
  $userid = $_SESSION["loggedinuserID"];
  function sortByFinal($a, $b) {
      return $b['final'] - $a['final'];
  }



  //getting all campaign weeks
      // defining
          $_weeks=array();

      // get current week id for checking purposes
          $week = ORM::for_table(null)->raw_query('SELECT `id`, `weekname` , date(start) as `start`, date(end) as `end` FROM week where now() >= start and now() <= end')->find_one();
          $thisweekid = $week['id'];

      // for each week
          $weeks = ORM::for_table('week')->order_by_asc('start')->find_many();
          foreach($weeks as $w)
          {
            $_weeks[$w['id']]['id']=$w['id'];
            $_weeks[$w['id']]['name']=$w['weekname'];
            $_weeks[$w['id']]['start']=$w['start'];
            $_weeks[$w['id']]['end']=$w['end'];
            $_weeks[$w['id']]['winners'] = array();
            
            //calculate the results for all users during this week tell now
            //2- for each user     
                $users = ORM::for_table('user')->find_many();
                $_users = array();
                $me = '-1';
                foreach($users as $u)
                { 
                  //get basic info for this user to use it later
                  //echo '<h4>Getting info for each user</h4>';
                  $_users[$u['id']]['userid']=$u['id'];
                  $_users[$u['id']]['dominos_email']=$u['dominos_mail'];
                  $_users[$u['id']]['picture']=$u['picture'];
                  $_users[$u['id']]['name']=$u['name'];
                  $_users[$u['id']]['score_of_game']=0;
                  $_users[$u['id']]['score_of_share']=0;

                  //how many pizzas have he bought last week?
                  $multiplier = 1;
                  if($u['games']>0)
                  {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://api.dominos.com.my/api/GameContest/GetCustomerCheeseBurstInfo');
                    curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
                    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 7);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch,CURLOPT_POST,true);
                    $data = array('Email' => $u['dominos_mail'], 
                                    'AppKey' => 'DominosFBGame',
                                    'StartDate' => $w['start'].' 00:00:00', 
                                    'EndDate' => $w['end'].' 23:59:59');                                               
                    $data_string = json_encode($data);    
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                        'Content-Type: application/json',                                                                                
                        'Content-Length: ' . strlen($data_string))                                                                       
                    );
                    if( ! $output = curl_exec($ch)) 
                    { 
                        echo curl_error($ch); 
                    }
                    $largePizzas = json_decode($output)->CheeseBurstLargeTotalCount;
                    $regularPizzas = json_decode($output)->CheeseBurstRegularTotalCount;
                    $pizzas = (2 * $regularPizzas) + (3 * $largePizzas);
                    if($pizzas >= 2)
                    {
                      $multiplier=$pizzas;
                    }
                    curl_close($ch);
                  }
                    //set the multiplier for last week for this user depending of the number of pizzas that he has bought
                    $_users[$u['id']]['multiplier'] = $multiplier;

                  if($u['games']>0)
                  {
                    //get the score of last week sharing for this user
                    $sql2 = "select ifnull(sum(T.score),0) as `score` from (SELECT `score`,`score_source`,Date(`score_date`)
                                                 FROM user_score where
                                                 user_id = ".$u['id']." 
                                                 and (`score_source` = 'Share twitter' or `score_source` = 'Share facebook' )
                                                 and (`score_date` between '".$w['start']." 00:00:00' and '".$w['end']." 23:59:59') group by score_source,DATE(`score_date`)) T";
                    $sharescore = ORM::for_table(null)->raw_query($sql2)->find_one();
                    $_users[$u['id']]['score_of_share'] = $sharescore['score'];


                    //get the high score of last week games for this user
                    $sql = "SELECT max(`score`) as `score` FROM user_score where `score_source`='game' and (`score_date` between '".$w['start']." 00:00:00' and '".$w['end']." 23:23:59') and `user_id` = ".$u['id'];
                    $gamescore = ORM::for_table(null)->raw_query($sql)->find_one();
                    $_users[$u['id']]['score_of_game'] = $gamescore['score'];
                  }
                  else
                  {
                    $_users[$u['id']]['score_of_share'] = 0;
                    $_users[$u['id']]['score_of_game'] = 0;
                  }

                    //calculate the final score for the user tell now
                    $_users[$u['id']]['final']=  ( $_users[$u['id']]['score_of_game'] + $_users[$u['id']]['score_of_share'] ) * $_users[$u['id']]['multiplier'];

                    //assign current user details into a var to be seen outside this loop
                    if($u['id']==$userid)
                    {
                      $me = $_users[$u['id']];
                    }

                  }

                  //sort users from hight score to low score
                  // echo 'before sorting: <br/>';
                  // foreach($_users as $q)
                  // {
                  //   echo $q['final'],'<br/>';
                  // }
                  usort($_users, 'sortByFinal');
                  //echo 'after sorting: <br/>';
                  // foreach($_users as $q)
                  // {
                  //   echo $q['final'],'<br/>';
                  // }
                  // var_dump($_users);



            // get this week winners
            $_winnersofthisweek = array();
            $i=0;
            foreach($_users as $q)
            {
              if($i < 10)
              {
                if($q['final'] > 0)
                {
                  array_push($_winnersofthisweek, $q);
                  $i++;
                }
              }
            }

            // if there is logged in user 
            // be sure that that user in the leaderboard array
            // for every week

            if($userid != null)
            {
              //check if the user isnide the array
              $amIinArray = false;
              foreach($_winnersofthisweek as $wotw)
              {
                if($wotw['userid']==$userid)
                {
                  $amIinArray=true;
                }
              }

              //var_dump($me);
              //if i am not in array add me
              // and sort the array again
              if(!$amIinArray && $me!= '-1')
              {
                  array_push($_winnersofthisweek, $me);
                  usort($_winnersofthisweek, 'sortByFinal');
              }
            }



            $_weeks[$w['id']]['winners'] = $_winnersofthisweek;
            $_weeks[$w['id']]['active'] = ($w['id'] == $thisweekid);

            // var_dump($_weeks[$w['id']]);
            // echo '<br/>';
            //echo json_encode($_weeks);
          }
          //die();
?>
<!doctype html>

<html lang="en">
   <head>

      <link href="http://allfont.net/allfont.css?fonts=arial-narrow" rel="stylesheet" type="text/css" />

      <meta charset="utf-8">
      <title>Win 1 Year Suppply of Pizza!</title>
      <meta name="description" content="Domino's">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <link href="assets/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/hover-min.css">

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
      


      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.rwdImageMaps.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
      <script src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>

      <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
     

      <meta property="og:title" content="Win 1 Year Suppply of Pizza!" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="http://bit.ly/DomiknowItAll" />
      <meta property="og:description" content="Are you a DomiKnow-It-All? Play now to beat my score!" />

      <meta name="twitter:card" value="Are you a DomiKnow-It-All? Play now to beat my score and win 1 year free pizza -> http://bit.ly/DomiKnowItAll">
     
     

   </head>

  <body>
    
    <div class="loading_div"></div>

    <div class="overlay_menu_div">
      <div class="headerMenuContainer">
        <a href="menu.php"><img src="assets/img/game/menu/play.png" /></a>
        <a href="index.php"><img src="assets/img/menu/home.png" /></a>
        <a href="howto.php"><img src="assets/img/game/menu/how.png" /></a>
        <a href="pointsystem.php"><img src="assets/img/menu/pointsystem.png" /></a>
        <a href="prizes.php"><img src="assets/img/game/menu/prizes.png" /></a>
        <!--<a href="topsecret.php"><img src="assets/img/game/menu/tsp.png" /></a>-->
        <a href="winners.php"><img src="assets/img/menu/winnerlist.png" /></a>
        <a href="leaderboard.php"><img src="assets/img/game/menu/leader.png" /></a>
        <a href="tac.php"><img src="assets/img/game/menu/tc.png" /></a>
      </div>
    </div>

    <div class="desktopContent hidden-xs">
        <div id="step2">
          <div id="game">
            <div class="headBar">
                <div class="left">
                  <div class="navbutton">
                    <a class="navicon-button x">
                      <div class="navicon"></div>
                    </a>
                  </div>
                  <!--<img id="goToPromotion" class="hvr-sink" src="assets/img/login/topleft1.png" />-->
                </div>
                <div class="right">
                <img class="playnowsmall hvr-float" src="assets/img/menu/playsmall.png" />
                </div>
                <img class="logo" src="assets/img/game/logo.png" />
            </div>
            <script type="text/javascript">
            $(function(){
              $('.playnowsmall').click(function(){
                window.location="game.php";
              });
            });
            </script>
          </div>
          
          <img class="logo img-responsive" src="assets/img/game/pages/leader.png" />
          
          <div class="leaderWeeks">
            <div data-weekid="1" class="singleWeek active">Week 1</div>
            <div data-weekid="2" class="singleWeek">Week 2</div>
            <div data-weekid="3" class="singleWeek">Week 3</div>
            <div data-weekid="4" class="singleWeek">Week 4</div>
          </div>


        </div>

        <div class="leaderContent">
            
           
          <h1 class="errmessageforwinners">No leaders yet</h1>
            
        </div>

        <img class="leaderdown" src="assets/img/game/pages/leaderdown.png" />
        <img class="hvr-float abitmoreupbro abitmorerightbro sharefacebook" src="assets/img/game/pages/leadersharefacebook.png" />
        &nbsp;
        <img class="hvr-float abitmoreupbro sharetwitter" src="assets/img/game/pages/leadersharetwitter.png" />
        <img class="hvr-float abitmoreupbro2 shareorder" src="assets/img/game/pages/leaderorder.png" />

    </div>
































    <div id="mobileGame" class="mobileContent visible-xs">
        <img id="mapfixer" class="img-responsive" src="assets/img/game/mobile/head0.png" usemap="#mobilemap" />
        <map name="mobilemap">
          <area shape="rect" coords="0,21,33,46" class="order" href="menu.php">
        </map>
        <img class="playerImage" />
        <p class="playerName2"></p>
        
          <img class="logo img-responsive" src="assets/img/game/pages/leader.png" />
          
          <div class="leaderWeeks">
            <div data-weekid="1" class="singleWeek active">Week 1</div>
            <div data-weekid="2" class="singleWeek">Week 2</div>
            <div data-weekid="3" class="singleWeek">Week 3</div>
            <div data-weekid="4" class="singleWeek">Week 4</div>
          </div>


        <div class="leaderContent">
            
            <h1 class="errmessageforwinners">No leaders yet</h1>

            
        </div>

      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 
      <div>&nbsp;</div> 

    </div>

    <!-- <img class="circlelogoutbtn btn-menu-mobile visible-xs" src="assets/img/logoutcircle.png" /> -->

      
    <script type="text/javascript">
      // closing button for desktop menu
      !function(n,r){"function"==typeof define&&define.amd?define(r):"object"==typeof exports?module.exports=r():n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},f=function(n,r,f){var c=(f?"remove":"add")+"EventListener",u=e(n),s=u.length,a={};for(var l in t)a[l]=r&&r[l]?o(r[l]):t[l];for(;s--;)for(var d in a)for(var v=a[d].length;v--;)u[s][c](a[d][v],i)},i=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return f(r,t),n},n.remove=function(r,t){return f(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});

      // extending sessionStorage
      Storage.prototype.setObj = function(key, obj) {
        return this.setItem(key, JSON.stringify(obj))
      };

      Storage.prototype.getObj = function(key) {
        return JSON.parse(this.getItem(key))
      };


      //facebook api
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1487607144892582',
          xfbml      : true,
          version    : 'v2.4'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));


      // global variables
      var user=sessionStorage.getObj('tjuser');
      
      var shareLink = 'http://bit.ly/DomiknowItAll';
      var shareTitlef = 'Are you a DomiKnow-It-All? Play now to beat my score!';
      var shareTitlet = 'Are you a DomiKnow-It-All? Play now to beat my score and win 3 years free pizza!';

      // //prevent cheating
      // if(user == null)
      // {
      //   logoutFromTheGame();
      // }
     
      // load user info
      if(user != null)
      {
        $('.playerImage').attr('src',user.picture);
        $('.playerName').html(user.first_name+'<br/> <span class="logoutBtnDesign2">LOGOUT</span>');
        $('.playerName2').html(user.first_name+' | <span class="logoutBtnDesign2">LOGOUT</span>');
      }

      function logoutFromTheGame()
      {
        user = '-1';
        ;sessionStorage.setObj('tjuser',null);
        $('body').removeClass("loading");
        $('#step2').fadeOut('slow',function(){
          window.location = 'logout.php';
        });

        $('#step2mobile').fadeOut('slow',function(){
          window.location = 'logout.php';
        });
      }

      
      var weeks = <?php echo json_encode($_weeks); ?>;
      var thisweekid = <?php echo $thisweekid; ?>;
      console.log(weeks);
      
      $(function(){






            $('.singleWeek').click(function(e){
            var id = $(this).data('weekid');
            console.log(id);
            for(var i in weeks)
            {
              if(i==id)
              {

                if(weeks[i].id > thisweekid)
                {
                  // this week is in future
                  // console.log('This week is in future!');
                  // $('.desktopContent .leaderContent').html('<h1 class="errmessageforwinners">This week is in future!</h1>');
                  // $('#mobileGame .leaderContent').html('<h1 class="errmessageforwinners">This week is in future!</h1>');
                  return;
                }

                $('.singleWeek').removeClass('active');
                $(this).addClass('active');

                if(weeks[i].winners.length < 1)
                {
                  //no leaders for this week yet;
                  console.log('No leaders for this week yet');
                  $('.desktopContent .leaderContent').html('<h1 class="errmessageforwinners">No Leaders yet</h1>');
                  $('#mobileGame .leaderContent').html('<h1 class="errmessageforwinners">No leaders yet</h1>');
                  return;
                }


                //set the winners
                
                $('.desktopContent .leaderContent').html('');
                $('#mobileGame .leaderContent').html('');
                for(var j in weeks[i].winners)
                {
                  
                  $('.desktopContent .leaderContent').append('<div class="leader">'+
                                                                          '<p class="number">'+(Number(j)+1)+'</p>'+
                                                                          '<img class="picture" src="'+weeks[i].winners[j].picture+'" />'+
                                                                          '<p class="name">'+weeks[i].winners[j].name+'</p>'+
                                                                          '<p class="score">'+weeks[i].winners[j].final+'</p>'+
                                                                        '</div>');
                  
                  $('#mobileGame .leaderContent').append('<div class="leader">'+
                                                                          '<p class="number">'+(Number(j)+1)+'</p>'+
                                                                          '<img class="picture" src="'+weeks[i].winners[j].picture+'" />'+
                                                                          '<p class="name">'+weeks[i].winners[j].name+'</p>'+
                                                                          '<p class="score">'+weeks[i].winners[j].final+'</p>'+
                                                                        '</div>');
                }


              }
            }
          });

          $('.singleWeek[data-weekid="<?php echo $thisweekid; ?>"]').trigger('click');
          $('.singleWeek').removeClass('active');
          $('.singleWeek[data-weekid="<?php echo $thisweekid; ?>"]').addClass('active');











          $('#mapfixer').rwdImageMaps();

          $('.navicon-button').click(function(e){
            e.preventDefault();
            $(this).toggleClass("open");
            $('body').toggleClass('menushown');
          });

          $('.overlay_menu_div').click(function(e){
            $('.navicon-button').trigger('click');
          });

          $('.logoutBtnDesign2,.circlelogoutbtn,.circlelogoutbtn2').click(function(e){
            logoutFromTheGame();
          });

          $('.facebookconnect').click(function(e){
            e.preventDefault();
            $('body').addClass("loading");
            window.location='game.php';
          });

          // $('.singleWeek').click(function(e){
          //   e.preventDefault();
          //   $('.singleWeek').removeClass('active');
          //   $(this).addClass('active');
          // });

          $('#goToPromotion').click(function(e){
            e.preventDefault();
            window.location.href = 'index.php?1';
          });




          $('.shareorder').click(function(e){
            e.preventDefault();
             window.top.location.href = 'https://www.dominos.com.my/meals?utm_source=Game&utm_medium=Leaderboard&utm_campaign=DomiKnowItAll';
          });


















          $('.sharefacebook').click(function(e){
            e.preventDefault();
              $('body').addClass("loading");
              FB.ui(
                {
                  method: 'share',
                  href: shareLink
                },
                // callback
                function(response) {
                   $('body').removeClass("loading");
                  if (response && !response.error_message) {

                    // add one more live because of sharing
                    if(user != null)
                    $.post('engine/userSharedFacebook.php',{u:user.email},function(data){},"json");
                    //window.location='game.php';
                    noty({
                      type:'success',
                      text:'Successfully shared! 10 points and 1 life will be added to your account in the next round.',
                      timeout: 7500,
                      layout:'topCenter',
                       animation: {
                          open: 'animated bounceInLeft',
                          close: 'animated zoomOutLeft',
                          easing: 'swing',
                          speed: 500
                      }
                    });
                    return;

                  } else {

                    noty({
                      type:'warning',
                      text:'Your post hasn\'t been shared. Please <b>Share it</b> to get more lifes.',
                      timeout: 7500,
                      layout:'topCenter',
                       animation: {
                          open: 'animated bounceInLeft',
                          close: 'animated zoomOutLeft',
                          easing: 'swing',
                          speed: 500
                      }
                    });
                    return;

                  }
                }
              );
          });

          $('.sharetwitter').click(function(e){
            e.preventDefault();
            //alert('share twitter not implemented yet');
            $('body').addClass("loading");
            var win = window.open("https://twitter.com/intent/tweet?text="+encodeURIComponent(shareTitlet)+"&url="+ encodeURIComponent(shareLink)+"");
            var timer = setInterval(function() {   
                if(win.closed) {  
                    clearInterval(timer);  
                    // add one more live because of sharing
                    if(user != null)
                    $.post('engine/userSharedTwitter.php',{u:user.email},function(data){},"json");
                    //window.location='game.php';
                    $('body').removeClass("loading");
                    noty({
                      type:'success',
                      text:'Successfully tweeted! 10 points and 1 life will be added to your account in the next round.',
                      timeout: 7500,
                      layout:'topCenter',
                       animation: {
                          open: 'animated bounceInLeft',
                          close: 'animated zoomOutLeft',
                          easing: 'swing',
                          speed: 500
                      }
                    });
                    return;

                }  
            }, 1000);
          });












      });
    </script>


  </body>
</html>
    
