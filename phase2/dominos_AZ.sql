SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE TABLE `gifts` (
  `id` int(20) NOT NULL,
  `user_id` int(15) NOT NULL,
  `gamedatetime` datetime NOT NULL,
  `copun` varchar(400) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `leaders` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `score` int(40) NOT NULL,
  `picture` varchar(10000) NOT NULL,
  `weekid` int(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `temp_results` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `score` int(40) NOT NULL,
  `picture` varchar(10000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `user` (
  `id` int(15) NOT NULL,
  `facebook_id` varchar(200) NOT NULL,
  `email` varchar(500) NOT NULL,
  `dominos_mail` varchar(500) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `picture` varchar(5000) NOT NULL,
  `phone` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `user_lifes` (
  `id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `life` int(1) NOT NULL,
  `source` varchar(100) NOT NULL,
  `life_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `user_score` (
  `id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `score_source` varchar(100) NOT NULL,
  `score` varchar(100) NOT NULL,
  `score_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



CREATE TABLE `week` (
  `id` int(15) NOT NULL,
  `weekname` varchar(100) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


INSERT INTO `week` (`id`, `weekname`, `start`, `end`) VALUES
(1, 'Week 1', '2015-09-17 00:00:00', '2015-09-23 23:59:59'),
(2, 'Week 2', '2015-09-24 00:00:00', '2015-09-30 23:59:59'),
(3, 'Week 3', '2015-10-01 00:00:00', '2015-10-07 23:59:59'),
(4, 'Week 4', '2015-10-08 00:00:00', '2015-10-14 23:59:59'),
(0, 'Development week', '2015-09-01 00:00:00', '2015-09-17 23:59:59');

CREATE TABLE `winners` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `score` int(40) NOT NULL,
  `picture` varchar(10000) NOT NULL,
  `weekid` int(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `gifts`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_lifes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_score`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `week`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `winners`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `gifts`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `user`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

ALTER TABLE `user_lifes`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

ALTER TABLE `user_score`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

ALTER TABLE `week`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

ALTER TABLE `winners`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
